package com.example.sarr.canadaapp;
/**
 * Ce Programme est une intervalle est une application android qui fourni
 * des informations générales sur les provinces du Canada.
 * Il a été élaboré par des étudiants au Baccalauréat en Informatique
 * de l'Université de Montréal . Hiver 2018.
 * Willy FOADJO:  Matricule :20059876
 * Abdramane Diasso: Matricule 20057513
 * Mohamed Sarr : Matricule 20050326
 */

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
/*
Classe de l'activité histoire
 */

public class Histoire extends AppCompatActivity {

    TextView textView_hist;

    MainActivity mainActivity = new MainActivity();

    static int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_histoire);

        i = mainActivity.getIndexClick();
        textView_hist = (TextView) findViewById(R.id.histoire);

        switch (i) {

            case 0:
               textView_hist.setText("Histoire de l'Alberta\n" +
                       "Commerçant de fourrures, années 1890 \n" +
                       "Alexander Rutherford, premier premier ministre de l'Alberta \n" +
                       "XVIIIe siècle\n" +
                       "Quand les missionnaires et les commerçants de fourrure sont arrivés de l'Europe durant le XVIIIe siècle, \n" +
                       "l'Alberta était habitée par plusieurs nations amérindiennes.\n" +
                       "Les Français et les Britanniques se  sont souvent disputée (fait marquant de l'histoire du Canada).\n" +
                       "Elle était donc peu exploitée au début jusqu'à ce qu'il y ait la création de postes de commerce près d'Edmonton.\n" +
                       "XIXe siècle\n" +
                       "Avec l'arrivée du Canadian Pacifique Railway (chemin de fer Canadien Pacifique),\n" +
                       "complété en 1885, et d'une poussée démographique,un gouvernement territorial fut créé en 1875.\n" +
                       "Les troupeaux de bisons dont les Amérindiens dépendaient pour leur survie ont été chassés \n" +
                       "jusqu'à la presqu'extinction et les terres ont été distribuées à des colons européens.\n" +
                       "Plusieurs années plus tard, cette région dirigée par un gouvernement territorial fut \n" +
                       "séparée en quatre portions dont une qui a été nommée d'après la Princesse Louise Caroline \n" +
                       "Alberta, fille de la Reine Victoria et du Prince Albert.\n" +
                       "Quand le Canada a été formé en 1867, les frontières de l'Alberta étaient encore menacées et non contrôlées.\n" +
                       "Ce n'est qu'en 1874 que la Gendarmerie à cheval du Nord-Ouest (précurseurs de la présente Gendarmerie royale du Canada)\n" +
                       "a établi sa présence sur ce territoire. L'Alberta fut admise comme étant une province du Canada en 1905.\n" +
                       "L'établissement de l'Alberta comme étant une province est dû en grande partie à Sir Frederick Haultain un avocat,\n" +
                       "également membre du Conseil des Territoires du Nord-Ouest, qui a beaucoup négocié pour que le statut de province \n" +
                       "soit octroyé partout dans l'Ouest. Pourtant, son but n'était pas vraiment de faire reconnaître l'Alberta ou la \n" +
                       "Saskatchewan comme une province mais bien de créer une énorme province de l'Ouest qui se serait appelée « Buffalo ». \n" +
                       "Le Premier Ministre de l'époque, Sir Wilfrid Laurier, ne voulait pas donner trop de puissance à l'Ouest Canadien \n" +
                       "et a donc opté pour un scénario dans lequel il y aurait deux provinces dans l'Ouest.\n" +
                       "Laurier octroya alors à Alexander Rutherford le titre de Premier ministre de l'Alberta.\n" +
                       "XXe siècle\n" +
                       "On découvrit du pétrole en Alberta dans les années 1920. En dépit du fait que quelques nappes furent \n" +
                       "exploitées par exemple à Turner Valley ou à Borradalle, le pétrole n'est pas devenu une source significative \n" +
                       "de revenus pour la province, du moins pas avant que l’Imperial Oil's Leduc #1 s'y installa le 13 février 1947.\n" +
                       "C'est à partir de ce moment que le « boom » du pétrole débuta en Alberta et est devenu un gage de prospérité.\n" +
                       "Leduc #1, continua d'extraire du pétrole jusqu'en 1974.\n" +
                       "Les libéraux formèrent le premier gouvernement de l'Alberta et restèrent au pouvoir mandat après mandat jusqu'en 1921.\n" +
                       "Cette année-là, les United Farmes furent élus et gardèrent le pouvoir jusqu'en 1934,\n" +
                       "lorsqu'ils furent défaits, probablement en raison du scandale sexuel entourant un des députés.\n" +
                       "En 1934, le parti Social Credit a été élu. Le Social Credit était basé sur des théories économiques \n" +
                       "excentriques du Britannique Major Douglas : il ramenait les certificats de prospérité (social credit), \n" +
                       "appelés monnaie amusante (funny money) par les gens de la province.\n" +
                       "Après son élection, le gouvernement du Social Credit fit plusieurs actions comme octroyer \n" +
                       "les certificats de prospérité et instaurer une censure de la presse.\n" +
                       "Quelques-unes de ces actions furent déclarées anticonstitutionnelles. \n");
                break;

            case 1:
                textView_hist.setText("Histoire de la Colombie-Britannique\n" +
                        "La Colombie-Britannique est la province la plus occidentale du Canada.\n" +
                        "Elle a joint la confédération canadienne le 20 juillet 1871.\n" +
                        "\n" +
                        "Avant la colonisation\n" +
                        "Les Autochtones de la côte du Nord-Ouest de l'Amérique et la région du fleuve Fraser\n" +
                        "La Colombie-Britannique, avant l'arrivée des Européens, était le foyer de plusieurs\n" +
                        "tribus aborigènes qui parlent plus de 30 différentes langues1.\n" +
                        "Les Amérindiens se divisaient en plusieurs regroupements linguistiques dont les Salish,\n" +
                        "les Kwakwaka'wakw (ou Kwakiutl), les Haïdas, les Kutenai, les Tsimshians et les Wakash.\n" +
                        "Tous ces peuples sont connus pour le potlatch et les totems.\n" +
                        "Le Salish se divisèrent en deux groupes : Salish de l'intérieur, \n" +
                        "tels que le peuple Okanagan ou Secwepemc (Shuswap);\n" +
                        "et les Salish de la côte, qui étaient très nombreux avant l'introduction des maladies \n" +
                        "européennes grâce à l'abondance de nourriture. Parmi les Salish de la côte on retrouve\n" +
                        "le peuple Musqueam (dans la région de la ville de Vancouver), le peuple Squamish\n" +
                        "(de North Vancouver et de la ville moderne de Squamish), le Sto:lo sur la région\n" +
                        "Lower Mainland, et les Cowichan, Snuneymuxw (Nanaimo), Comox et Sencoten (Saanich),\n" +
                        "parmi autres, sur l'île de Vancouver. Dans la région du détroit de Johnstone vivait le peuple Kwakwaka'wakw. Sur la côte ouest de l'île de Vancouver vivaient les Nuu-chah-nulth, dont le chef Maquinna accueillit les navigateurs espagnols, britanniques et américains au XVIIIe siècle. Les Tsimshian et les Haïda dominaient les côtes du nord, se concentrant en villages de grandes maisons de cèdre, ornés de totems. Les Haïda étaient basés sur les îles de la Reine-Charlotte, connus aussi aujourd'hui par le nom « Haida Gwaii ». Les Haïda, comme les Tsimshian et ses voisins les Tlingit, se servaient de grands canots de mer pour voyager au continent et vers les îles de l'Alaska actuel et à l'île de Vancouver, où ils faisaient des raids pour emporter des individus Kwakiutl ou Salish comme esclaves. À l'intérieur les Amérindiens demeuraient dans des « quiggly-holes », des maisons demi-souterraines. Une échelle faite du tronc d'un arbre donnait accès à l'intérieur.\n" +
                        "Les explorations européennes\n" +
                        "Les premières navigations européennes connues du côté Pacifique de ce qui est\n" +
                        "aujourd'hui la Colombie-Britannique furent des projets espagnols.\n" +
                        "Un explorateur grec, Juan de Fuca, au service des Espagnols, fit rapport aux Anglais\n" +
                        "d'avoir fait un voyage au nord du Mexique en 1592, pendant lequel il découvrit un détroit\n" +
                        "dans la côte pacifique entre les 47e et 48e parallèles2. En 1774 Juan José Péres Hernandez \n" +
                        "vit l'île de Vancouver et les îles de la Reine-Charlotte. L'Espagne se considérait alors en possession de toute la côte pacifique au nord de l'Alaska Pourtant, l'empire russe était en train d'étendre ses explorations de la côte de l'Alaska. Les Espagnols et les Russes étaient suivis de près par le navigateur britannique James Cook en 1778, qui fit contact avec les Amérindiens à la baie Nootka. Un voyage français en 1786, sous la direction de Jean-François de Galaup, comte de La Pérouse, cherchait des territoires ouverts à la colonisation française et tentait de fixer l'existence de la grande « mer de l'Ouest » qui figurait sur les cartes françaises depuis plus d'un siècle3. Après 1783 les empires européens furent joints dans le nord-ouest du continent américain par des navigateurs-commerçants de la Nouvelle-Angleterre. Pendant les années 1780 et 1790 du XVIIIe siècle les Espagnols, les Russes, les Britanniques et les Américains se faisaient concurrence pour la traite de la fourrure de loutre de mer, alors très prisée en Chine. À partir de 1789 un conflit entre la Grande-Bretagne et l'Espagne menaçait d'éclater en guerre ouverte. En 1792 George Vancouver et Juan Francisco de la Bodega y Quadra, des représentants de leurs pays respectifs, se rencontrèrent à la baie Nootka sur l'île de Vancouver pour discuter une résolution. L'Espagne se retira alors en Californie, laissant champ libre aux Britanniques et aux Américains. Ces derniers devenaient de plus en plus agressifs, surtout après le célèbre voyage d'exploration de Lewis et Clark qui ont atteint l'embouchure du fleuve Columbia en 1805.\n" +
                        "Alexander Mackenzie, un Canadien né en Écosse, accompagné de six voyageurs canadiens-français, un autre Écossais et un guide amérindien, furent les premiers Européens à atteindre l'océan Pacifique en traversant l'Amérique du Nord au nord de Rio Grande. Il le fit en 1793 et il vit le Pacifique à partir du présent village de Bella Coola, inscrivant sur une roche les mots : « Alexander Mackenzie, from Canada, by Land » (Alexandre Mackenzie, du Canada, par Terre) ainsi que la date de son arrivée4,5. D'autres explorateurs tels que Simon Fraser, qui suivit le fleuve Fraser jusqu'au détroit de Géorgie (1806-1808)6, et David Thompson aidèrent à l'établissement des postes de traite de fourrure dans l'ouest du continent7. Bien que Mackenzie et Fraser aient trouvé le Pacifique, ils trouvèrent les routes impossibles pour le commerce. C'est David Thompson qui a découvert le fleuve Columbia et qui l'a suivi jusqu'à son embouchure, atteignant le Pacifique en 1811. Par contre, il fut incapable de s'approprier le territoire puisque les explorateurs américains Lewis et Clark y étaient arrivés les premiers en 1805. Ils avaient planté un drapeau américain et étaient partis. John Jacob Astor avait fondé le village d'Astoria quelques mois avant que Thompson arrive.\n" +
                        "Le jargon chinook, un combinaison de mots amérindiens, français et anglais se développa pour permettre le commerce entre les autochtones et les employés de la HBC. Même aujourd'hui en Colombie-Britannique et dans l'État de Washington plusieurs mots de Chinook s'utilisent encore par les anglophones: « skookum » (grand; merveilleux, correct), « high mucketymuck » (des personnes d'importance); « chuck » (l'eau); « saltchuck » (la mer) et « tyee » (chef, monarque, leader). La reine Victoria était connue aux Amérindiens par le titre « hyas klootchman tyee » (lit.: grande femme monarque)\n" +
                        "Après cette période, la Colombie-Britannique, connue alors des Britanniques sous les noms de « Columbia District » et « New Caledonia District » (le district de la Nouvelle-Calédonie en français) fut en grande partie dirigée par la Compagnie de la Baie d'Hudson. Fort Victoria (future capitale de la C.-B.) fut établie en 1843 comme étant un point stratégique de défense pour les affaires de la HBC (Hudson Bay Company). En 1844, le parti démocratique des États-Unis affirma que les États-Unis avaient un droit légitime de posséder ce qu'ils appelaient l'Oregon au complet, mais le président James Polk était préparé à couper la frontière au 49e parallèle. Quand les Britanniques refusèrent l'offre, n'étant pas prêts à abandonner les territoires entre l'embouchure du fleuve Columbia et le 49e parallèle, Polk mit fin au négociations et les Américains continuèrent de prôner que le territoire leur appartenait. Ils lançaient des slogans comme \"Fifty-four forty or fight\" (54,40 ou la guerre). Mais la guerre entre les États-Unis et le Mexique prit beaucoup l'attention et les ressources des Américains qui en sont donc venus à se préparer à faire un compromis.\n" +
                        "Les batailles pour la frontière de l'Oregon ont abouti en 1846 avec le traité d'Oregon. Celui-ci mentionnait que la frontière entre l'Amérique du Nord britannique et les États-Unis était au 49e parallèle à partir des montagnes rocheuses jusqu'à la mer.\n" +
                        "Les colonies de l'île de Vancouver, des îles de la Reine-Charlotte et de la Colombie-Britannique\n" +
                        "Le gouvernement de Grande-Bretagne créa la colonie de la couronne de l'île de Vancouver et en 1849, en 1851, Douglas fut nommé gouverneur. En 1852, une rumeur d'une découverte d'or sur les îles de la Reine-Charlotte pousse Douglas à convaincre Londres de former une seconde colonie afin de contrôler les mineurs venant de la Californie pour empêcher les Américains de s'en emparer8. Pendant les années 1850 le nombre de colons britanniques sur l'île de Vancouver demeurait très modeste : quelque 400 personnes à Victoria, des mineurs de charbon à Nanaimo et quelques colons. Les gens de Victoria se plaignaient que le gouverneur Douglas mettait toujours les intérêts commerciaux de la Compagnie de la Baie d'Hudson, dont il était toujours l'employé, avant ceux de la colonie. De sa part, pour hausser les revenus de la colonie, Douglas imposait des taxes sur la vente d'alcool, action peu populaire parmi les commerçants et les propriétaires de tavernes. À part la population d'origine européenne (Anglais et Écossais, mais qui comptait un nombre important de Canadiens-français, employés de la CHB) il y avait des Kanakas, de Hawaïi ainsi, bien sur, une grande population d'Amérindiens Tsimshians, Nuu-chah-nulth et Kwakwaka'wakw. En tant que gouverneur, Douglas s'efforçait de maintenir des relations cordiales avec les Amérindiens et conclut plusieurs traités pour accommoder les besoins des colons et de la Compagnie de la Baie d'Hudson.\n" +
                        "La ruée vers l'or\n" +
                        "En 1858 Douglas se proclama gouverneur de la Colombie-Britannique lors de la ruée vers l'or dans le canyon Fraser. Douglas, déjà gouverneur de l'île de Vancouver et des îles de la Reine-Charlotte, s'inquiétait que les mineurs et marchands américains, qui se précipitaient de la Californie vers les colonies britanniques, étaient assez nombreux pour saisir le contrôle de ces territoires pour les États-Unis9. La ruée vers l'or changea profondément le caractère des colonies britanniques de la côte du Pacifique. En premier, l'arrivée du premier vaisseau de San Francisco doublait immédiatement la population de Victoria, qui se transforma pendant l'été de 1857 d'un village de frontière aux prétentions britanniques à une ville de tentes avec une population mixte d'Américains, Allemands, Français, Italiens, Scandinaves, dont nombre avaient des histoires criminelles ou violentes. Des colons noirs, voulant s'établir dans un pays autre que les États-Unis pour échapper à l'intolérance américaine de l'avant-guerre, arrivèrent à l'invitation du gouverneur, qui avait lui-même quelques ancêtres de descendance d'esclaves des colonies britanniques des Caraïbes.\n" +
                        "Dans la Colombie-Britannique continentale, les Canadiens-français étaient les plus nombreux avant 1858 et l'arrivée des mineurs. En 1858, des groupes d'hommes armés se groupèrent en milices informelles et tuèrent plusieurs Amérindiens Okanagans10. Une guerre entre les Blancs et les Amérindiens ayant déjà éclaté dans le territoire de Washington, au sud de la Colombie-Britannique, Douglas voulait éviter un conflit violent dans les territoires qu'il dirigeait. Face aux nouveaux arrivés, Douglas devait se comporter de façon décisive pour imposer la loi britannique dans la région. Les Britanniques envoyèrent alors les « Royal Engineers » (ingénieurs royaux) un régiment militaire qui avait aussi l'expérience dans les travaux publics. Ayant établi la paix entre les Amérindiens et les mineurs américains, les Royal Engineers se mirent au développement de la ville de New Westminster qui fut établie comme première capitale de la Colombie-Britannique. Les plus grandes découvertes d'or pendant la ruée vers l'or eurent lieu à Barkerville dans la région du Cariboo, à l'intérieur central de la colonie.\n" +
                        "Entrée dans le Canada\n" +
                        "Les raisons de la décision des Britanno-Colombiens de se joindre au dominion du Canada en 1871 furent nombreuses. Il y avait la peur d'annexion aux États-Unis, la dette écrasante créée par la croissance rapide de la population et le besoin de services gouvernementaux pour les supporter, et la fin de la ruée vers l'or et la dépression légère qui l'avait accompagnée.\n" +
                        "La Colombie-Britannique devint province du dominion du Canada le 20 juillet 1871, à la suite de la promesse du dominion de construire une ligne de chemin de fer reliant la côte pacifique aux provinces de l'est.\n" +
                        "Chemin de fer\n" +
                        "Le Canadien Pacifique à travers les montagnes Rocheuses fut difficilement construit entre 1875-1885. La construction tardive poussa l'Assemblée Législative à Victoria à voter une résolution unanime de pétitionner la reine Victoria pour la sécession de la province de la Confédération canadienne. Enfin le gouvernement de Macdonald commença la construction. Le chemin de fer, qui devait se terminer à Victoria, aboutit finalement à la baie Burrard, au petit village de Granville, renommé Vancouver. La construction à travers les montagnes était dure et dangereuse. Pour compléter le nombre d'ouvriers et réduire les coûts du projet le gouvernement du Dominion permit à la compagnie Canadien Pacifique de faire venir en Colombie-Britannique environ 15 000 ouvriers chinois11. Les gages des Chinois n'étaient que les deux-tiers des gages des ouvriers blancs et plusieurs périrent à cause des accidents.");
                break;

            case 2:
                textView_hist.setText("Histoire de l'Île-du-Prince-Édouard\n" +
                        "\n" +
                        "L’Histoire de l'Île-du-Prince-Édouard peut être divisée en quatre périodes : les premiers temps, l'Acadie, la colonie britannique et la confédération canadienne qu'elle a rejoint le 1er juillet 1873.\n" +
                        "\n" +
                        "Les premiers temps\n" +
                        "\n" +
                        "L'Île-du-Prince-Édouard était à l'origine habitée par les Micmacs. Ils l'appelaient « Abegweit » qui veut dire « bercer sur les vagues ». Ils croyaient que l'île avait été formée par le Grand Esprit déposant de l'argile rouge qui aurait pris sa forme sur les flots bleus.\n" +
                        "\n" +
                        "L'Acadie\n" +
                        "\n" +
                        "Comme partie de la colonie française de l'Acadie, et, après 1713, de la colonie de l'Île Royale, l'île était appelée « Île Saint-Jean ». Quelque mille Acadiens vivaient sur l'île. Par ailleurs, plusieurs débarquèrent sur l'île venant de la Nouvelle-Écosse au moment de la déportation des Acadiens en 1755. Ils étaient cinq mille.\n" +
                        "\n" +
                        "En 1758, les soldats britanniques, sous le commandement du colonel Andrew Rollo, reçurent l'ordre du général Jeffery Amherst de prendre l'île. Plusieurs Acadiens furent déportés. Après avoir quitté l'île, trois cents soixante moururent lorsque le navire Duke William coula avec deux autres navires, le Violet (280 moururent) et le Ruby (213 moururent) le 13 décembre 1758, en route de Île St.-Jean vers la France1.\n" +
                        "La colonie britannique\n" +
                        "\n" +
                        "La nouvelle colonie britannique de « St. John's Island », aussi connue comme « Island of St. John » fut établie par des familles recherchant l'élégance sur mer. l'île devint au XVIIIe siècle un lieu de retraite paisible pour la noblesse britannique.\n" +
                        "En 1798, la Grande-Bretagne changea le nom de « St. John's Island » pour « Prince Edward Island » afin de la distinguer de noms semblables dans l'Atlantique. Le nouveau nom honore le quatrième fils du roi George III, le prince Édouard Auguste, duc de Kent (1767-1820), qui commandait alors les troupes britanniques à Halifax.\n" +
                        "\n" +
                        "La confédération canadienne\n" +
                        "\n" +
                        "En septembre 1864, l'Île-du-Prince-Édouard était l'hôte de la Conférence de Charlottetown qui était la première rencontre d'un processus dirigeant les Articles de la Confédération et la création du Canada en 1867. L'Île-du-Prince-Édouard n'a pas trouvé les termes de l'union favorables et refusa de s'y joindre en 1867, choisissant de rester une partie de la nation de la Grande-Bretagne et d'Irlande. À la fin des années 1860, la colonie examina diverses options, incluant la possibilité de devenir un État indépendant, comme celle de rejoindre les États-Unis d'Amérique.\n" +
                        "Au début des années 1870, la colonie commença la construction d'un chemin de fer et frustrée par la Grande-Bretagne commença des négociations avec les États-Unis. En 1873, le premier ministre John A. Macdonald, inquiet de l'expansionnisme américain et cherchant une distraction du Scandale du Pacifique, négocia pour que l'Île-du-Prince-Édouard joigne le Canada. Le gouvernement fédéral du Canada assuma la dette du chemin de fer de la colonie et accepta de financer le rachat des terres des propriétaires absents afin de libérer l'île de la tenure en loyer et pouvoir les vendre aux nouveaux immigrants. L'Île-du-Prince-Édouard entra dans la confédération le 1er juillet 1873.\n" +
                        "\n" +
                        "Pour avoir été l'hôte de la rencontre inaugurale de la confédération, la Conférence de Charlottetown, l'Île-du-Prince-Édouard se présente comme le « lieu de naissance de la confédération ». Plusieurs édifices, un traversier, et le Pont de la Confédération, le pont le plus long sur les eaux glacées au monde, utilisent le mot « confédération » de plusieurs façons. L'édifice le plus éminent dans la province avec ce nom est le « Confederation Centre of the Arts », présenté comme un cadeau aux Prince-Édouardiens par les 10 gouvernements provinciaux et le gouvernement fédéral lors du centenaire de la Conférence de Charlottetown, à Charlottetown, comme monument national aux Pères de la confédération.");
                break;

            case 3:
                textView_hist.setText("Histoire du Manitoba\n" +
                        "\n" +
                        "L'espace géographique qu'occupe actuellement le Manitoba était habité dès que les glaciers de la dernière ère glaciaire se sont retirés vers le sud-ouest.\n" +
                        "La première région qui aurait été habitée serait Turtle Mountain, où on a retrouvé plusieurs dessins des hommes de l'époque. Les premiers humains au sud du Manitoba ont laissé de la poterie, des flèches de fer, des dessins, des os de poissons et d'animaux. Des signes qui pourraient nous faire croire qu'il y aurait eu de l'agriculture ont été retrouvés sur le long de la Red River près de Lockport.\n" +
                        "\n" +
                        "Plus tard se sont installés des groupes d'aborigènes plus récents comme les Ojibwa, les Cree, les Dene, les Sioux, les Mandan, les peuples Assiniboine, avec d'autres tribus qui sont entrées dans la région pour marchander. Il y avait beaucoup de routes qui faisaient partie d'un plus large réseau de commerce autant sur la terre que sur l'eau. Dans la région de Whiteshell Provincial Park et de la rivière Winnipeg on a retrouvé beaucoup de dessins sur pierre des amérindiens ce qui porte à croire que cet endroit aurait été un centre commercial ou même un endroit où apprendre et partager le savoir pendant plus de 1000 ans. Les coquilles du Cypraeida et le cuivre ont probablement été marchandés dans un réseau encore plus grand de commerce qui s'étendrait aussi loin qu'aux peuples près du Mississippi et au Nouveau-Mexique et même sur l'océan.\n" +
                        "\n" +
                        "En 1611, Henry Hudson fut un des premiers européens à naviguer dans ce qu'on appellera plus tard la baie d'Hudson. Le Nonsuch était le premier bateau qui ait navigué dans la baie d'Hudson en 1668-1669 qui a mené à la création de la Compagnie de la Baie d'Hudson. On a octroyé à la Compagnie de la Baie d'Hudson les droits de possession de la fourrure dans tout le réseau hydrographique de la baie d'Hudson (qui couvre maintenant ce qui est connu comme étant Alberta, Saskatchewan, Manitoba, Ontario, Minnesota, le Dakota du Nord, et plus encore. Ce réseau hydrographique a été baptisé Terre de Rupert suite à la contribution du prince Rupert lors de la formation de la compagnie de la baie d'Hudson. \n" +
                        "\n" +
                        "D'autres marchands et explorateurs provenant des petites îles britanniques ont ensuite navigué dans la baie d'Hudson et plusieurs rivières du nord du Manitoba. Le premier européen à avoir mis pied à terre au Manitoba (central) actuel fut Sir Thomas Button, qui a voyagé le long de la rivière Nelson et du Lac Winnipeg en 1612. Il a probablement rejoint également la région des prairies puisque dans son voyage il a rapporté avoir vu un bison. Pour ce qui est des explorateurs français, Pierre Gaulthier de Varennes et Sieur de la Vérendrye ont visité la vallée de la rivière Rouge dans les années 1730. Plusieurs autres Français et Métis viendront de l'est et du sud par la rivière Winnipeg et la rivière Rouge.\n" +
                        "\n" +
                        "Une population assez importante de canadiens français (les franco-manitobains) habite encore au Manitoba. Ils habitent en plus grande concentration dans le district de Saint-Boniface à l'est de Winnipeg. Plusieurs postes de traite de fourrures furent bâtis par la compagnie du Nord-Ouest et la compagnie de la baie d'Hudson près des rivières et des lacs. Il y avait beaucoup de compétition entre ces compagnies, surtout au sud.\n" +
                        "\n" +
                        "Il y a quelques possibilités de source pour le nom \"Manitoba\". La première serait les mots assiniboines \"mini\" et \"tobow\" qui veulent dire \"Lac de la prairie\". L'autre qui est le plus susceptible d'être la source est le mot Cree \"maniotwapow\" qui vient du mot \"Manitou\".\n" +
                        "Le territoire du Manitoba a été gagné par le royaume de Grande-Bretagne en 1763 lors de la guerre avec les Français. Ce territoire était une partie de la Terre de Rupert, l'immense terrain de traite monopolistique de fourrure détenu par la compagnie de la baie d'Hudson.\n" +
                        "\n" +
                        "Celui-ci comptait tous les territoires du réseau hydrographique relié à la baie d'Hudson. La plupart des rivières du Manitoba coulent vers le nord, plutôt que vers le sud et l'est comme ce que les gens pensent en général, et se jettent dans la baie d'Hudson. Les archives de la baie d'Hudson (Hudson Bay Archives) sont situés à Winnipeg, au Manitoba.\n" +
                        "Ils préservent l'histoire de l'ère de la traite de fourrures qui a eu lieu près de la plupart des rivières qui passent dans la terre de Rupert.");
                break;

            case 4:
                textView_hist.setText("Histoire du Nouveau-Brunswick\n" +
                        "\n" +
                        "Le Nouveau-Brunswick proprement dit a été fondé en 1784 avec pour capitale Fredericton. Cet article présente l'histoire du lieu, depuis l'époque amérindienne\n" +
                        "\n" +
                        "jusqu'à nos jours, incluant l'époque coloniale française où il était une partie de l'Acadie.\n" +
                        "\n" +
                        "L'historiographie de la préhistoire du Nouveau-Brunswick se subdivise généralement en trois périodes: la période précoce (-9000 à -4000), la période intermédiaire (-4000 à -500) et la période tardive (-500 à 1600)note 1.\n" +
                        "Les artéfacts les plus anciens, datant du Xe millénaire av. J.-C., ont été découverts dans la paroisse de Pennfield en 20111.\n" +
                        "Contact (1000 à 1604)\n" +
                        "\n" +
                        "Reconstitution de L'Anse-aux-Meadows.\n" +
                        "En 985 ou 986, le Viking Erik le Rouge explore le sud du Groenland. Bjarni Herjólfsson tente de le rejoindre mais est dirigé plus au sud, probablement à Terre-Neuve, devenant possiblement le premier Européen à explorer l'Amérique2. Vers l'an mille, Leif Ericson, le fils d'Erik le Rouge, explore les terres découvertes par Bjarni Herjolfsson. Il fonde Straumfjord, dans le Nord de la colonie du Vinland, correspondant à L'Anse aux Meadows, à Terre-Neuve-et-Labrador ; la colonie est abandonnée quelques années plus tard2. La Saga d'Erik le Rouge mentionne aussi la fondation d'une autre colonie, Hop, une guerre avec la population locale, les Skrælings, et l'exploration d'autres terres mais aucune preuve concluante de leur présence ailleurs qu'à L'Anse-aux-Meadows n'a été trouvée jusqu'à ce jour2. On sait toutefois que les Vikings ont continué de visiter occasionnellement l'Amérique du Nord pour se procurer du bois2.\n" +
                        "\n" +
                        "Entre 1075 et 1080, Adam de Brême est le premier à mentionner le Nord-est du continent, dans Gesta Hammaburgensis ecclesiae pontificum.\n" +
                        "Dès le XVIe sièclenote 2, des pêcheurs basques viennent régulièrement pêcher la baleine et la morue à Terre-Neuve3. Au contact des Amérindiens, un pidgin basco-algonquin se développe entre les Basques et les Micmacs. Aux caraques basques, encore très majoritaires au XVIIe siècle, s'ajoutent quelques navires portugais, français, bretons et normands ainsi que des marchands de fourrures4,5.\n" +
                        "\n" +
                        "L'Amérique du Sud est colonisée par l'Espagne et le Portugal à la suite du voyage de Christophe Colomb, en 1492 ; le traité de Tordesillas, signé en 1494, concède tout le continent à ces deux puissances6. L'Angleterre souhaite alors coloniser l'Amérique du Nord, aux environs du 37e parallèle, tandis que la France s'intéresse au 45e parallèle. Jean Cabot est probablement le premier à explorer l'Acadie pour le compte de l'Angleterre, en 14976. La France, constamment en conflit avec l'Espagne, s'intéresse tardivement à la colonisation des Amériques ; Giovanni da Verrazzano est toutefois envoyé pour explorer la côte de la Floride à l'île du Cap-Breton en 1524. Arrivé dans la région de l'actuelle ville de Washington, il utilise pour la première fois le nom « Acadie », sous la forme « Arcadie », en référence à cette région luxuriante de Grèce ; le terme est appliqué plus au nord dès le XVIIe siècle6.\n" +
                        "En juillet 1534, Jacques Cartier explore la côte entre la baie de Kouchibouguac et la baie des Chaleurs7. Il rencontre des Micmacs à Gaspé, avec qui il fait du troc7. Son voyage permet de mieux connaître la géographie et les cultures amérindiennes mais aussi de confirmer la possibilité d'un établissement permanent et l'abondance des ressources naturelles ; le passage tant recherché vers l'Asie n'est toutefois pas trouvé6.\n" +
                        "\n" +
                        "Premières tentatives\n" +
                        "En raison des problèmes de territoire causés par la traite des fourrures, le roi de France Henri III accorde des monopoles de traite à des groupes de marchands à partir de 1588, dans le but de financer la colonisation ; en 1603, le marchand protestant Pierre Dugua de Mons obtient un monopole de dix ans à condition d'établir plusieurs colons8. À la même époque, des écrivains font miroiter une vision idyllique de l'Amérique8.\n" +
                        "Troilus de Mesgouez fonde une colonie à l'île de Sable en 1598 mais celle-ci est abandonnée en 1603, douze hommes seulement ayant survécu9. Aymar de Chaste obtient le monopole commercial de la Nouvelle-France la même année ; il envoie en expédition François Gravé, accompagné entre autres de Samuel de Champlain, mais aucun établissement n'est fondé.\n" +
                        "\n" +
                        "En 1604, De Mons part en expédition, accompagné d'environ 80 personnes dont Samuel de Champlain et Jean de Poutrincourt9. De Mons choisit de s'établir à la baie française, appelée baie de Fundy de nos jours, à la suggestion de Champlain ; la colonie est fondée à l'île Sainte-Croix, sur la rive nord, mais environ la moitié des colons meurent du scorbut lors du premier hiver9. La colonie est déplacée l'année suivante à Port-Royal, sur la rive sud ; douze personnes meurent tout de même du scorbut mais la colonie reçoit l'aide des Micmacs9. Champlain explore toute la côte jusqu'au cap Cod9. La colonie coûte cher à entretenir et il est presque impossible d'empêcher la contrebande d'autres marchands8. Le monopole commercial de De Mons est révoqué avant échéance en 1607 et il ramène tous les colons en France.\n" +
                        "\n" +
                        "Poutrincourt obtient une concession et revient s'établir à Port-Royal en 1610, accompagné de quelques personnes dont Claude et Charles de Saint-Étienne de La Tour9. Le commerce de fourrures ne parvient pas à combler les coûts de la colonie et Poutrincourt demande l'aide financière des Jésuites ; il leur demande aussi d'envoyer deux des leurs pour seconder le prêtre Jessé Fleché9. À leur arrivée, ils accusent ce dernier d'avoir baptisé les Micmacs sans leur avoir donné l'instruction religieuse nécessaire ; l'affaire divise la colonie en deux camps et prend une telle proportion qu'elle est présentée à la cour et à la Sorbonne9. Les Jésuites fondent en 1613 une colonie rivale sur l'île des Monts Déserts9. Peu après l'arrivée de Poutrincourt, des Malouins sous le commandement du capitaine Merville fondent une autre colonie rivale sur l'île Emenenic, désormais l'île Caton, dans le fleuve Saint-Jean10.\n" +
                        "Colonisation écossaise\n" +
                        "\n" +
                        "\n" +
                        "La période qui s'ensuit n'est pas bien connue mais l'on sait que les Français habitent parmi les indigènes et font venir annuellement des vivres de La Rochelle11 tandis que leur isolement jette les bases de l'identité acadienne12. Robert Gravé du Pont, fils de François, enlève vraisemblablement une Malécite, envenimant les relations avec Port-Royal13. Biencourt envoie alors des troupes au fleuve Saint-Jean et la colonie rend les armes après une faible résistance13. L'île Emenenic est vraisemblablement abandonnée dans les années suivantes13.\n" +
                        "En 1621, le gouvernement anglais change le nom de la colonie en Nouvelle-Écosse mais ne s'y intéresse réellement qu'à partir de 1629 lorsqu'il fait venir les colons écossais de William Alexander ; Claude de la Tour est alors fait baronnet et reçoit une grande terre. La France s'intéresse à nouveau à l'Acadie à la même époque ; le gouvernement abandonne les compagnies privées alors que Richelieu fonde la Compagnie des Cent-Associés en 1627, à laquelle l'État participe et dont l'un des objectifs est de faire venir un grand nombre de colons. En 1631, Charles de la Tour est nommé lieutenant général de l'Acadie par la France ; grâce à l'aide gouvernementale, il construit en 1632 un fort au cap Sable et un autre à Saint-Jean. \n" +
                        "\n" +
                        "Retour à la France\n" +
                        "L'Acadie est cédée à la France en 1632 par la signature du traité de Saint-Germain-en-Laye, qui met aussi fin à la colonisation écossaise15. Le gouvernement français tente de transformer l'Acadie en un « rempart » entre le Canadanote 3 et la Nouvelle-Angleterre et pour cette raison implante le système seigneurial puis nomme Isaac de Razilly au poste de gouverneur16. Le régime seigneurial est en grande partie un échec, dû notamment à la grande liberté de la population, au faible contrôle du gouvernement et à l'étendue du territoire17.\n" +
                        "L'Acadie est à nouveau délaissée pendant plusieurs années car la Guerre franco-espagnole se déroule entre 1636 et 1659 tandis que la première Révolution anglaise dure de 1642 à 166016. Entretemps, Razilly déplace la capitale à La Hève ; il s'intéresse plus au commerce maritime qu'à l'agriculture, ce qui explique ses choix d'établissements.\n" +
                        "\n" +
                        "Guerre civile\n" +
                        "\n" +
                        "Françoise-Marie Jacquelin défendant le fort Saint-Jean contre D'Aulnay, par Charles William Jefferys.\n" +
                        "La mort de Razilly, survenue en 1636, provoque une dispute entre Charles de Menou d'Aulnay de Charnizay et Charles de Saint-Étienne de la Tour. D'Aulnay ramène la capitale à Port-Royal et déclenche une guerre civile contre La Tour15. Tous deux s'adressent au roi à plusieurs reprises pour faire trancher les limites de leur territoires respectifs ; la décision rendue confond les deux territoires — preuve de la faible connaissance géographique de l'Acadie — et ne règle pas leur querelle16. D'Aulnay et La Tour concluent des ententes avec le Massachusetts mais les Anglais évitent d'être trop impliqués dans le conflit19. Ils en profitent toutefois pour ramener le premier chargement de charbon de la mine de Minto, la première en Amérique du Nord, en 164313.\n" +
                        "D'Aulnay de Charnizay meurt accidentellement en 1650, causant une guerre de succession entre Emmanuel Le Borgne, Charles de Saint-Étienne de la Tour et Nicolas Denys ; Le Borgne était le principal créancier de la famille D'Aulnay mais n'arrive pas à s'entendre sur la succession19. En 1652, il s'empare de Port-Royal, où se trouvent les intérêts de la famille D'Aulnay, et attaque les établissements de ses rivaux, dont La Hève, Pentagouët et Havre-Saint-Pierre20. La Tour épouse la veuve de D'Aulnay à la fois pour tenter de réconcilier les deux familles, de rétablir la paix et pour reprendre ses possessions20. En 1654, Denys obtient une concession comprenant le golfe du Saint-Laurent entre le Canceaux et Gaspé.\n" +
                        "\n" +
                        "Contrôle anglais\n" +
                        "\n" +
                        "Robert Sedgwick, qui a pour mission d'attaquer la Nouvelle-Néerlande, attaque aussi l'Acadie sur son passage en 165420. Durant les années suivantes, la France, malgré la perte de l'Acadie, continue d'accorder des concessions ainsi que des permis de chasse et de pêche20. Le Royaume-Uni renomme la colonie Nouvelle-Écosse et la concède à William Crowne, Charles de Saint-Étienne de la Tour et Thomas Temple20. La Tour profite peu de cette concession, alors que Temple, devenu plus tard gouverneur, fait peu d'efforts pour mettre en valeur son territoire alors qu'il est continuellement opposé à ses rivaux comme Emmanuel Le Borgne20. Il construit toutefois le premier fort anglais à Jemseg en 165921.\n" +
                        "Les colonies nord-américaines en 1664.\n" +
                        "\n" +
                        "Situation de la Nouvelle-France.\n" +
                        "\n" +
                        "Le traité de Bréda rend l'Acadie à la France en 1667 ; le gouverneur Hector d'Andigné de Grandfontaine n'en prend cependant le contrôle effectif qu'en 1670 car l'ancien gouverneur Thomas Temple cause toutes sortes de problèmes22. Accompagné de 30 soldats et de 60 colons, Grandfontaine doit rétablir l'autorité français auprès des 400 Acadiens, habitués depuis une décennie à vivre de façon indépendante, et empêcher les activités des pêcheurs et marchands de la Nouvelle-Angleterre. Il semble que ni Grandfontaine ni ses successeurs ne parviennent à atteindre ces deux objectifs, pourtant considérés comme nécessaires au contrôle de l'Acadie par la France. La Compagnie des Cent-Associés croule sous les dettes et est remplacée en 1664 par la Compagnie des Indes occidentales, elle-même dissoute en 1674. L'Acadie est alors rattachée au domaine royal. Le gouverneur de l'Acadie est désormais nommé par le roi et révoqué à sa guise. Disposant de quelques fonctionnaires, il est secondé par un commissaire ; l'Acadie est en fait une subdivision de la Nouvelle-France, dont dépendent les institutions et la défense. Toutefois, la grande distance de Québec à Port-Royal fait que la colonie fait souvent affaire directement avec la France22. L'administration est gênée par la nouvelle politique de colonisation française et la colonie n'ayant pas de garde côtière, les pêcheurs continuent leurs opérations sans être dérangés23. En fait, la France se désintéresse de l'Acadie jusqu'à la fin du XVIIe siècle. La colonie a peu de financement et des troupes insuffisantes. Les gouverneurs, mal payés, déplacent souvent la capitale et, comme dans les autres colonies, certains font même de la contrebande avec les Anglais22.\n" +
                        "\n" +
                        "En 1674, le néerlandais Jurriaen Aernoutsz attaque Pentagouët et pille plusieurs villages sur son passage. Le gouverneur Jacques de Chambly se rend après deux heures de combat tandis que son lieutenant, situé à Jemseg, est fait prisonnier. Bernard-Anselme de Saint-Castin reprend Pentagouët en 167924. Le gouverneur Pierre de Joybert de Soulanges et de Marson meurt en 1678. Frontenac désire étendre son contrôle sur l'Acadie et nomme Leneuf de La Valière au titre de gouverneur mais cette décision n'est pas entérinée par le roi. Face aux critiques, il est remplacé en 1684 par François-Marie Perrot24.\n" +
                        "Première Guerre intercoloniale\n");
                break;

            case 5:
                textView_hist.setText("Histoire de la Nouvelle-Écosse\n" +
                        "\n" +
                        "Les premières traces de peuplement humain dans l’actuelle Nouvelle-Écosse suivent immédiatement le retrait des glaciers lors de la fin de la dernière période glaciaire, soit il y a 10 000 à 13 000 ans. Il s’agit d’outils de pierre et de vestiges de campements de chasseurs-cueilleurs de la période paléoindienne (13 000 à –9 000 ans avant ce jour).\n" +
                        "\n" +
                        "Étrangement, il existe très peu de traces de peuplement humain dans la période située entre 10 000 et 5 000 ans avant ce jour. On parle de cette époque comme du « grand hiatus ». Plusieurs hypothèses d’ordre surtout écologique ont été avancées pour expliquer cette absence mais aucune n’a pu être démontrée.\n" +
                        "Par contre, la période immédiatement postérieure, qui va jusqu’à il y a 2 500 ans révèle de nombreuses traces de peuplement humain Les outils de pierre réapparaissent alors et on trouve parmi eux de nombreux agrès de pêche, indiquant une prépondérance croissante des ressources maritimes dans l’alimentation. \n" +
                        "\n" +
                        "Se développe alors la culture dite « archaïque maritime », présente depuis beaucoup plus longtemps au Labrador, à Terre-Neuve et sur la côte du Maine. Outre un important outillage pour la pêche et la chasse aux mammifères marins (poids, harpons, hameçons) une grande variété d’outils conçus pour le travail du bois se trouve dans les sites de cette civilisation. Les rites funéraires élaborés ont laissé des tombeaux dans lesquels les défunts étaient enterrés, recouverts d’ocre rouge et accompagnés d’outils, d’amulettes et de sculpture en pierre représentant souvent des animaux marins. La disparition de cette culture pourrait être reliée à une hausse du niveau de la mer, survenue il y a 3 500 ans, qui submergea l’actuel plateau continental, jusqu’alors émergé. Les sites les plus intéressants se trouveraient alors sous l’eau.\n" +
                        "\n" +
                        "Vers 3 000 ans avant aujourd’hui, la région fut occupée par des peuples de la tradition dite « Susquehanna » venus du sud. Leur mode de vie associait chasse, pêche et cueillette. Vers 2 500 ans, et jusqu’à l’époque du contact avec les Européens (XVIe siècle), c’est la période « céramique », correspondant à l’arrivée des peuples Micmacs et Malécites, et se distinguant, comme son nom l’indique, par l’apparition de la poterie, innovation venue du sud comme, sans doute, les Mi'kmacs eux-mêmes si on en croit leurs traditions orales. La langue micmaque appartient à la famille algonquienne, comme le malécite, l’abénaki, le cri, l’innu et la plupart des langues parlées dans le nord-est de l’Amérique.\n" +
                        "\n" +
                        "Au XVIe siècle, les Micmacs entrent dans une phase d’expansion et leur territoire couvrira, à l’arrivée des Européens, environ 100 000 km² dans les trois Provinces maritimes et la Gaspésie. Ce sont eux que les Français appelleront les « Gaspésiens ». Ce territoire sera divisé en 7 districts. Ces districts reconnaissent chacun un « sagamo » ou chef dont la principale fonction est l’attribution des territoires de chasse et de pêche. Il n’a guère d’autres pouvoirs et, pour le reste l’organisation politique micmaque est semblable à celle des autres peuples algonquiens de l’est canadien : on vit en villages restreints comprenant quelques familles et le rôle de chef est reconnu, de façon temporaire et informelle, aux individus faisant preuve de compétences reconnues par tous. L’institution du « sagamo », toutefois, est particulière aux Micmacs et pourrait indiquer une influence méridionale, de même que la place importante tenue par le culte du soleil dans leur religion. Par ailleurs, celle-ci comporte un recours au shaman, comme chez les autres peuples algonquiens. \n" +
                        "\n" +
                        "Les bandes se regroupent en été pour la pêche et se divisent en hiver pour chasser à l’intérieur des terres. La cueillette et, par endroit, une agriculture d’appoint, permettent de combler les besoins. L’habitation est composée de cabanes en perches recouvertes d’écorces, certains villages côtiers sont entourés de palissades.\n" +
                        "\n" +
                        "La population totale est difficile à évaluer. Les estimés les plus en vogue parlent de 5 à 6 000 personnes mais, selon certains auteurs, il y a pu y avoir jusqu’à 35 000 Micmacs juste avant le contact avec les Européens. Il est sûr que les guerres et les épidémies des trois siècles qui suivirent réduisirent considérablement la population autochtone, ici comme ailleurs en Amérique.\n" +
                        "\n" +
                        "Premières explorations\n" +
                        "Il est difficile de dire avec certitude qui fut le premier Européen à toucher les côtes de la Nouvelle-Écosse. Outre les spéculations autour de la venue de Scandinaves médiévaux, alimentées par la découverte d’une monnaie norvégienne du XIIe siècle dans l’État du Maine, il n’est pas du tout certain que Giovanni Cabotto (John Cabot) ait touché terre à l’endroit qui s’appelle aujourd’hui Cabot’s trail, lors de son voyage de 1497. Il a, en tout cas, longé la côte et, ayant identifié les Grands Bancs de morue de Terre-Neuve, il a suscité l’invasion de pêcheurs européens dans le Golfe du Saint-Laurent qui se produisit au XVIe siècle.\n" +
                        "\n" +
                        "En 1524, Giovanni Verrazano, au service du roi de France, cartographia la côte atlantique de l’Amérique du Nord et lui donna le nom « Arcadie », inspiré de la mythologie antique (nous sommes à la Renaissance). Ce terme donna naissance au nom « Acadie ». Il est possible que le nom se soit confondu avec le terme d’origine micmaque « cadie » qui désignerait un port. Le Florentin fut suivi de près par le portugais Gomes, au service de l’Espagne.\n" +
                        "La présence accrue des pêcheurs et baleiniers européens dans les eaux du nord-est américain eut pour conséquence le développement du commerce des fourrures avec les Autochtones et, subséquemment, le développement de compagnies à monopole. Il s’ensuivit une volonté, de la part des puissances européennes, de s’assurer le contrôle des voies stratégiques permettant l’accès à ce commerce. La France désirant s’assurer le contrôle de l’embouchure du Saint-Laurent, l’aube du XVIIe siècle vit l’envoi de plusieurs expéditions chargées de trouver un emplacement favorable à l’établissement d’une colonie. La première tentative, à l’île de Sable en 1598, se termina en désastre et quelques survivants faméliques durent être rapatriés.\n" +
                        "\n" +
                        "Colonisation européenne\n" +
                        "En 1604, Pierre du Gua de Monts, marchand huguenot, fut chargé par Henri IV de la colonisation de ce qu’on appelait de plus en plus souvent l’Acadie. Associé au noble Jean de Poutrincourt et au géographe Samuel de Champlain, il procéda, cette année-là à la fondation de Port Royal, aujourd’hui Annapolis. Les contacts avec les Micmacs furent suffisamment bons pour que la colonie, abandonnée en 1607 par manque de vivres, soit laissée sous la garde du sagamo Membertou.\n" +
                        "\n" +
                        "Les Français étaient de retour en force en 1610. Les rivalités coloniales troublèrent rapidement la paix : dès 1613, une attaque virginienne ravagea la colonie et, en 1621, le roi Jacques VI d’Écosse accorda à sir William Alexander, une charte l’autorisant à établir une colonie écossaise en Acadie et le nom « Nova Scotia » (Nouvelle-Écosse) apparut sur les cartes.\n" +
                        "\n" +
                        "En 1628, les frères Kirke, corsaires britanniques, prirent la colonie et des colons Écossais débarquèrent. Trois ans plus tard, la colonie fut rendue à la France par traité et les Écossais furent expulsés pendant que 300 colons français, sous la direction du sieur Isaac de Razilly, venaient s’installer. La colonie relevait désormais de la Compagnie des Cents Associés, qui devait procéder à la colonisation, en échange du monopole de la traite des fourrures.\n" +
                        "Cela mettait la colonie sous la dépendance du gouverneur de la Nouvelle-France installé à Québec. Cependant, celui-ci était bien loin et l’Acadie fut, de fait, laissée à un abandon presque total. Aussi, lorsque Razilly mourut, la même année, et que son domaine fut partagé entre les sieurs Charles de Menou d'Aulnay et Charles La Tour, les deux seigneurs entrèrent en une rivalité qui dégénéra vite en une véritable guerre privée qui culmina avec le massacre de la garnison du fort La Tour, par d’Aulnay, en 1645. La Tour intriguant avec les Anglais, une force anglaise prit la colonie en 1654. Rendue à la France en 1667, la colonie fut de nouveau l’objet d’attaques anglaises en 1690, et puis d'attaques britanniques en 1707 et en 1710.\n" +
                        "\n" +
                        "Les Traités d'Utrecht (1713) cédèrent l’actuelle Nouvelle-Écosse, à l’exception de l’île Royale (aujourd’hui île du Cap Breton) à la Grande-Bretagne. Les régions qui constituent aujourd’hui le Nouveau-Brunswick et l’île-du-Prince-Édouard restèrent à la France. Cependant, la majorité des 4 000 habitants désignés désormais comme Acadiens se retrouva sous domination britannique. Malgré toutes ces péripéties, la colonie avait relativement prospéré. Les Acadiens, contrairement à la plupart des colons européens en Amérique, avaient développé de nouvelles terres, non en défrichant la forêt, mais en gagnant sur la mer, grâce à un réseau de digues, les aboiteaux, et de canaux inspirés de la technique des marais poitevins, région dont plusieurs étaient originaires. Le développement de l’agriculture ainsi qu’un commerce important avec les colonies voisines, y compris britanniques malgré les guerres et les interdictions, permit à la population d’augmenter rapidement (ils seront 15 000 en 1755). Cette évolution était d’autant plus nécessaire que les pandémies successives qui frappèrent la population autochtone avaient réduits le nombre de Micmacs et Malécites au point que ceux-ci n’étaient plus en mesure de ravitailler la colonie. D’ailleurs le troupeau d’orignaux de l’île Royale fut exterminé au milieu du XVIIe siècle et la population micmaque de l’île émigra.\n" +
                        "\n" +
                        "La « Nouvelle-Écosse » et l’Acadie française\n" +
                        "\n" +
                        "Vue du débarquement des troupes de Nouvelle-Angleterre sur l'île de Cap Breton pour attaquer Louisbourg en 1745. (Dessin de 1747)\n" +
                        "Le siège de Louisbourg en 1745. (Gravure allemande anonyme du XVIIIe siècle)\n" +
                        "\n" +
                        "La période qui va de 1714 à 1755 fut celle d’un face-à-face stratégique entre grandes puissances. D’un côté, la France, désireuse de combler le vide laissé par l’abandon de sa portion de l’Acadie, investit dans le développement d’une nouvelle « Acadie française », dont le centre fut la ville-forteresse de Louisbourg, située sur l’île Royale, dont la construction commença en 1718. Les coûts de la construction furent exorbitants et les délais nombreux mais, en quelques décennies, Louisbourg devint, avec 4 000 habitants, la plus grande agglomération de l’empire français d’Amérique du Nord et un haut lieu du commerce transatlantique… ainsi que de la contrebande avec les colonies britanniques. Ceci bien que peu d’Acadiens ait quitté leurs terres pour s’établir dans cette île rocailleuse. Parallèlement, un début de colonisation française se fit aux îles de la Madeleine, à l’île Miscou et à l’île St-Jean, données d’abord en fief au comte de Saint-Pierre, puis royalisées en 1724.\n" +
                        "\n" +
                        "Du côté britannique, la colonie de Nouvelle-Écosse connut un développement important à travers l’augmentation de la population et le développement du commerce avec les colonies britanniques et françaises malgré l’interdiction de ce dernier par le Parlement britannique en 1722. Le principal problème tenait au statut incertain de la population acadienne, qui refusait en masse de prêter le serment d’allégeance à la couronne exigé par le gouvernement colonial. Cette situation incitait Londres à refuser à la colonie les institutions coutumières aux colonies britanniques (chambre d’assemblée, habeas corpus, etc.) et à maintenir un régime militaire. Les colons britanniques, par conséquent, tendaient à bouder la Nouvelle-Écosse qui demeurait une enclave française dans l’empire britannique.\n");
                break;

            case 6:
                textView_hist.setText("Histoire du Nunavut\n" +
                        "\n" +
                        "La région actuellement connue comme Nunavut est peuplée continuellement depuis 4 000 ans. La plupart des historiens identifient l'île de Baffin avec le Helluland des sagas scandinaves. Il est donc possible que les habitants de la région aient eu des contacts occasionnels avec des marins scandinaves. Voir aussi paléoeskimo et néoeskimo.\n" +
                        "\n" +
                        "L'histoire écrite du Nunavut commence en 1576. Martin Frobisher, en dirigeant une expédition pour trouver le passage du Nord-Ouest, crut avoir découvert du minerai d'or près de l'actuelle baie de Frobisher sur la côte de l'île de Baffin. Le minerai était sans valeur, mais Frobisher fut le premier contact européen connu avec les Inuit. Le contact fut agressif : Frobisher captura quatre Inuits et les ramena en Angleterre, où ils périrent.\n" +
                        "D'autres explorateurs vinrent au XVIIe siècle en quête du Passage du Nord-Ouest, dont Henry Hudson, William Baffin et Robert Bylot, ou encore, au XIXe siècle, l'expédition Franklin financée par la Couronne britannique. En 1871, les États-Unis subventionnent l'expédition Polaris menée par Charles Francis Hall pour trouver le Pôle Nord; puis Henry W. Howgate mène plusieurs expéditions scientifiques, visant entre autres à rencontrer les Inuits, atteignant la baie de Cumberland ainsi que le Groenland.\n" +
                        "\n" +
                        "En 1893, Joseph Burr Tyrrell et James Williams Tyrrell, employés par la Commission géologique du Canada pour explorer l'intérieur des terres du Keewatin, partirent du lac Athabasca situé à la frontière de l'Alberta et de la Saskatchewan, descendirent la rivière Dubawnt, traversèrent la région de Chesterfield Inlet et longèrent la côte de la baie d'Hudson jusqu'à Churchill. L'année suivante, Joseph Burr Tyrrell explora et cartographia la partie sud de l'intérieur des terres du Keewatin.\n" +
                        "\n" +
                        "Pendant la période de colonisation par les Canadiens, les Inuits ont été contraints à la sédentarisation à l'aide de méthodes coercitives, dont le massacre des chiens esquimaux.\n" +
                        "\n" +
                        "Contemporaine\n" +
                        "\n" +
                        "Des négociations pour un accord sur les revendications territoriales commencent en 1976 entre le gouvernement fédéral et le Tapirisat inuit du Canada. En avril 1982, une majorité des résidents des Territoires du Nord-Ouest votent en faveur de la scission, et le gouvernement fédéral l'approuve conditionnellement sept mois après. L'accord sur les revendications territoriales est conclu en septembre 1992 et ratifié par près de 85 % des électeurs du futur Nunavut. En juin 1993, le Parlement du Canada adopte la Loi concernant l’Accord sur les revendications territoriales du Nunavut et la Loi sur le Nunavut. La transition débouche sur la création du Nunavut le 1er avril 1999.");
                break;

            case 7:
                textView_hist.setText("Histoire de l'Ontario\n" +
                        "\n" +
                        "Avant l'arrivée des Européens, la région était habitée par les peuples algonquiens :\n" +
                        "les Saulteux,les Cris et les Algonquins) etiroquoïens (les Iroquois et les Hurons).\n" +
                        "\n" +
                        "Français et Anglais\n" +
                        "\n" +
                        "L'explorateur français Étienne Brûlé explora une partie de la région de 1610 à 1612. L'explorateur anglais Henry Hudson navigua sur la baie d'Hudson en 1611 et revendiqua les alentours pour l'Angleterre, mais Samuel de Champlain atteignit le lac Huron en 1615 et les missionnaires français commencèrent à établir des missions aux abords des Grands Lacs. La colonisation française fut entravée par les hostilités avec les Iroquois, qui s'allièrent plus tard aux Anglais.\n" +
                        "\n" +
                        "Compagnie de la Baie d'Hudson\n" +
                        "\n" +
                        "La Grande-Bretagne établit des comptoirs à la baie d'Hudson vers la fin du XVIIe siècle, commençant une lutte pour la domination de l'Ontario.\n" +
                        "\n" +
                        "Cession, annexion, scission\n" +
                        "\n" +
                        "Le traité de Paris en 1763 mit fin à la guerre de Sept Ans en cédant presque tout l'empire français en Amérique (la Nouvelle-France) aux Britanniques. La région aujourd'hui appelée Ontario fut annexée au Québec en 1774. La loi constitutionnelle de 1791 scinda le Québec en deux parties, les Canadas :\n" +
                        "Haut-Canada à l'ouest de la rivière des Outaouais et\n" +
                        "Bas-Canada à l'est.\n" +
                        "\n" +
                        "Rébellions\n" +
                        "\n" +
                        "Les troupes américaines de la guerre de 1812 incendièrent Toronto en 1813. Après la guerre, beaucoup d'immigrants britanniques vinrent s'installer en Haut-Canada, et commencèrent à s'irriter contre l'aristocratique Family Compact qui gouvernait la région, de même que la Clique du Château gouvernait au Bas-Canada. Alors, la rébellion en faveur du gouvernement responsable se leva aux deux régions, sous Louis-Joseph Papineau par les Patriotes canadiens-français au Bas-Canada, et sous William Lyon Mackenzie au Haut-Canada par les \"Patriots\" écossais.\n" +
                        "\n" +
                        "Province du Canada\n" +
                        "\n" +
                        "Bien que les deux rébellions fussent écrasées, le gouvernement britannique envoya lord Durham pour enquêter sur les causes des émeutes. Il recommanda l'octroi d'autonomie politique et la refusion des colonies afin d'assimiler les Québécois - les Britanniques du Haut-Canada étaient maintenant majoritaires aux Canadas. Les deux colonies furent alors fusionnées dans la Province du Canada en 1841, avec l'Ontario sous le nom de Canada-Ouest. Le gouvernement parlementaire autonome fut octroyé en 1849.\n" +
                        "\n" +
                        "Confédération\n" +
                        "\n" +
                        "Craignant une possible agression américaine causée par la guerre de Sécession, le Canada, le Nouveau-Brunswick et la Nouvelle-Écosse décidèrent de fusionner dans la fédération (nommée à tort confédération) en 1867. Le conflit soutenu entre les deux parties de la Province du Canada causa leur séparation : elles entrèrent elles aussi dans la fédération comme deux provinces distinctes, l'Ontario et le Québec.\n" +
                        "Nord-ouest\n" +
                        "À la fin du XIXe siècle la partie du nord-ouest fut attribuée à l'Ontario, mais il y reste un fort courant séparatiste qui voudrait qu'elle soit rattachée au \n" +
                        "\n" +
                        "Manitoba.\n" +
                        "\n" +
                        "Commençant avec la construction du chemin de fer transcontinental à travers les Grandes plaines jusqu'à la Colombie-Britannique, l'industrie ontarienne connut un grand essor. L'exploitation minière commença au début du XXe siècle.\n" +
                        "Arrivée de sièges sociaux\n" +
                        "Le mouvement nationaliste au Québec poussa plusieurs sociétés commerciales à migrer vers l'Ontario. Toronto remplaça alors Montréal comme Métropole et Centre économique du Canada.\n" +
                        "\n" +
                        "Partis politiques\n" +
                        "\n" +
                        "Les partis politiques provinciaux principaux sont les progressistes-conservateurs, les libéraux, et les néo-démocrates. Les droitistes progressistes-conservateurs de Mike Harris détrônèrent les gauchistes néo-démocrates en 1995 ; le gouvernement Harris mit en œuvre un programme libertarien de coupures dans les dépenses sociales et d'abaissement des taxes (la « Révolution du bon sens »). Cette politique équilibra le budget mais fut dénoncée pour avoir entraîné une hausse de la souffrance et de la pauvreté, surtout à Toronto. En particulier, les critiques de ce gouvernement blâment les coupures au ministère de l'Environnement pour son manque de surveillance, responsable de la « tragédie de Walkerton », une épidémie d'E. coli causée par l'eau contaminée à Walkerton, qui causa plusieurs morts et maladies en mai 2000. Harris quitta son poste en 2002 et fut remplacé par Ernie Eves. Les Conservateurs furent défaits l'année suivante par le Parti libéral aux élections de 2003. Le chef de ce parti, le premier ministre actuel, est Dalton McGuinty. Il fut réélu en 2006.");
                break;

            case 8:
                textView_hist.setText("Histoire du Québec\n" +
                        "\n" +
                        "Avant la colonisation française de l'Amérique (de la préhistoire à 1508)\n" +
                        "\n" +
                        "Article détaillé : Préhistoire et protohistoire du Québec.\n" +
                        "\n" +
                        "Selon la théorie du premier peuplement de l'Amérique, les premiers habitants sont arrivés en Amérique à partir de l'Asie il y a environ 30 000 ans en franchissant un isthme, c'est-à-dire une étroite bande de terre, qui reliait alors le Nord-Est de l'Asie au Nord-Ouest de l'Amérique, à l'endroit où se trouve aujourd'hui le détroit de Béring. Cet isthme était alors à sec à la suite de la baisse du niveau des océans causée par l'accumulation d'eau dans les immenses glaciers qui couvraient alors une grande partie de la planète.\n" +
                        "\n" +
                        "Des sites archéologiques au Sud du Québec démontrent que des groupes de chasseurs paléoindiens pénètrent dans la vallée du Saint-Laurent au moment où la mer de Champlain se retire, il y a environ 10 000 ans. Ils circulent sur le territoire avec une grande mobilité dans un environnement fort différent de celui d'aujourd'hui. La proximité des glaciers apporte un climat rigoureux. Le paysage végétal ressemble beaucoup à celui qu'on trouve en milieu nordique. Il semble qu'ils se déplacent à l'aide d'embarcations et utilisent des tentes en peau. Leurs proies préférées sont les grands cervidés, principalement les caribous, bien que les autres mammifères, de même que le poisson, complètent leur alimentation.\n" +
                        "\n" +
                        "Quelques millénaires plus tard, les Amérindiens de l'Archaïque étendent peu à peu leurs activités à toutes les ressources animales et végétales disponibles et vivent un nomadisme saisonnier adapté à la chasse, la pêche et la cueillette.\n" +
                        "\n" +
                        "Les outils se diversifient : les Amérindiens polissent leurs outils en pierre et martèlent le cuivre natif provenant de la région du lac Supérieur. La présence de minéraux provenant du Labrador (Ramah), de Pennsylvanie et de cuivre sur des sites du Québec démontre l'étendue des échanges et des communications, qui ne cessera de s'accroître jusqu'à l'arrivée des Européens.\n" +
                        "\n" +
                        "À partir de -3 000 ans, la poterie est adoptée dans la plaine laurentienne. La cueillette prend une place plus importante dans leurs activités, la chasse et la pêche n'étant cependant pas délaissées pour autant.\n" +
                        "\n" +
                        "C'est le début de l'expérimentation de la fabrication de la poterie, principalement dans la partie sud du Québec.\n" +
                        "C'est à cette époque que certaines populations amérindiennes provenant des Grands Lacs étendent leur réseau d'échanges à la région laurentienne.\n" +
                        "Il y a environ mille ans, les premiers Inuits sont arrivés sur le territoire du Québec et ont remplacé les Tunits, peuple aujourd'hui disparu. L'immigration s'est faite peu à peu, par de petits groupes comportant moins de 30 personnes.\n" +
                        "L'agriculture apparaît en Amérique du Nord de façon expérimentale vers le VIIIe siècle mais ce n'est qu'au XIVe siècle qu'elle est pleinement maîtrisée dans la vallée du Saint-Laurent. Les Iroquoiens du Saint-Laurent y cultivaient entre autres le maïs, la courge, le tournesol, et le haricot.\n" +
                        "Des Vikings se sont installés en Amérique vers l'an 1000 et on retrouve des traces de leur présence jusqu'en 1340, principalement à Terre-Neuve.\n" +
                        "Au début du XVIe siècle, alors que les Français entreprennent l'exploration de l'Amérique, il y a environ 30 000 Amérindiens sur le territoire de ce qui deviendra la province de Québec.\n" +
                        "\n" +
                        "La Nouvelle-France (1508-1763)\n" +
                        "\n" +
                        "Article détaillé : Histoire de la Nouvelle-France.\n" +
                        "Les explorations françaises sans colonisations réussies (1508-1607)\n" +
                        "Les expéditions de pêche[modifier | modifier le code]\n" +
                        "Des expéditions de pêche exploitaient les bancs de Terre-Neuve dès le XVe siècle. La première expédition française avérée date de 1508, soit seulement 16 ans après le premier voyage de Christophe Colomb. En effet, Thomas Aubert ramène quelques amérindiens en France. Cet événement confirme que dès le début du XVIe siècle, des navigateurs français s'aventuraient dans le golfe du fleuve Saint-Laurent.\n" +
                        "La présence de pêcheurs basques sera aussi attestée dans le compte-rendu des voyages de Jacques Cartier.\n" +
                        "Le voyage de Verrazano (1524)\n" +
                        "\n" +
                        "Article détaillé : Giovanni da Verrazano.\n" +
                        "\n" +
                        "Ce n'est cependant qu'en 1524 qu'un voyage officiel, commandité par des marchands et par le roi de France, est organisé.\n" +
                        "Cherchant un passage plus court vers l'Asie, Verrazano longe minutieusement la côte atlantique de l'Amérique du Nord de la Floride jusqu'à Terre-Neuve cherchant le passage tant convoité vers la Chine. Il rentra évidemment bredouille, mais son voyage a servi à préparer les voyages de Jacques Cartier, dix ans plus tard.\n" +
                        "\n" +
                        "Les voyages de Jacques Cartier (1534-1542)\n" +
                        "\n" +
                        "Article détaillé : Jacques Cartier.\n" +
                        "\n" +
                        "Portrait factice de Jacques Cartier, découvreur de la Nouvelle-France.\n" +
                        "\n" +
                        "Le roi de France, François Ier, veut se joindre aux nations qui ont commencé depuis peu à explorer l'Atlantique pour y trouver un chemin vers la Chine et les Indes. Il finance donc les voyages de Jacques Cartier et le charge de trouver « certaines îles et pays où l'on dit qu'il se doit de trouver de grandes quantités d'or, d'épices ainsi que de soies ». On dit que Jacques Cartier est le découvreur du Canada parce qu'il est le premier à explorer le territoire en vue de son exploitation systématique.\n" +
                        "\n" +
                        "Lors de son premier voyage en 1534, Jacques Cartier explore le golfe du fleuve Saint-Laurent mais croit que le détroit qui sépare l'île d'Anticosti de la péninsule de Gaspésie est une baie. Il rate donc la découverte du fleuve Saint-Laurent. Le 23 juillet, il plante une croix de neuf mètres de hauteur et revendique la baie de Gaspé au nom de la France.\n" +
                        "\n" +
                        "Visite de Hochelaga et du Mont Royal\n" +
                        "\n" +
                        "Plan La Terra De Hochelaga Nella Nova Francia, avec à gauche, le Monte Real.\n" +
                        "Lors de son second voyage en 1535, après s'être arrêté le 7 septembre à Québec, Cartier remonte le Saint-Laurent jusqu'à Hochelaga, maintenant la ville de Montréal. Le 3 octobre 1535, il est accueilli à Hochelaga qu'il visite, puis il monte sur la montagne située à proximité, qu'il nomme Mont Royal.\n" +
                        "Vingt et un ans plus tard, en 1556, la République de Venise, qui tout comme la France s'intéressait aux Indes, reproduit dans le 3e tome Delle Navigationi et Viaggi une illustration de la visite de Jacques Cartier à Hochelaga sur le plan La Terra De Hochelaga Nella Nova Francia. Chose surprenante, une analyse de cette illustration révèle que les trois collines du Mont Royal, montrées sur cette illustration, sont fidèlement reproduites. La visite de Hochelaga terminée, Cartier revient à Stadaconé qu'il atteint le 2 octobre où il passera l'hiver. Lors de son troisième voyage en 1541, Jacques Cartier, devenu subalterne de Roberval, fonde la colonie de Charlesbourg-Royal. L'année suivante Roberval arrive à Charlesbourg-Royal qu'il renomme France-Roy. Tous deux, ce séparément, reviennent à la bourgade Hochelaga alors détruite. Ils essaient de passer outre les saults1 qui avaient jusque-là bloqué l'avance dans l'intérieur des Indes occidentales.\n" +
                        "\n" +
                        "Sans cesse attaquée par les Indiens de Stadaconé, la colonie connaît des moments précaires. Déçu de n'avoir trouvé ni passage vers l'Asie, ni richesses, ni terres hospitalières, François Ier ne veut plus investir de sommes importantes dans une aventure d'exploration et de colonisation incertaine. Il commande le retour en France de la colonie. Les pêcheurs français continuent à pêcher dans le golfe du Saint-Laurent et à faire le commerce des fourrures avec les Amérindiens mais il faudra attendre plus de 60 ans avant qu'une autre tentative de colonisation ne soit entreprise.\n" +
                        "\n" +
                        "La colonisation\n" +
                        "\n" +
                        "À la suite des résultats décevants des voyages de Cartier où l'on n'avait trouvé ni métaux précieux ni passage vers l'Asie, le gouvernement français se désintéresse du Canada. Mais certains Français (Basques, Bretons et Normands) continuent de venir dans la région de Terre-Neuve pour la pêche à la morue. Ils viennent également pour la chasse à la baleine et aux loups-marins pour l'huile qu'on en tire pour s'éclairer. Contrairement à ceux qui font de la morue verte, ceux qui font sécher la morue doivent passer un certain temps en Amérique avant de retourner en Europe ce qui leur permet de procéder à des échanges avec les Autochtones qui convoitent les objets en métal des Européens. C'est ainsi que le commerce des fourrures commence. Peu à peu, ces échanges deviennent de plus en plus importants si bien que cela ranime l'intérêt pour ce coin de l'Amérique septentrionale. Ce sont les marchands qui en deviennent les principaux promoteurs et à la fin du XVIe siècle, on assiste aux premières tentatives d'établissements permanents en Acadie et dans la vallée du Saint-Laurent.\n" +
                        "Contrairement à la pêche, le commerce des fourrures nécessite une présence continue sur le territoire pour nouer des relations profitables avec les Autochtones qui font l'essentiel du travail dans cette activité commerciale. Mais des établissements permanents coûtent cher. C'est ainsi que l'État français a pris assez tôt l'habitude d'accorder des privilèges à des sociétés commerciales pour qu'elles financent ces premières installations. Habituellement, le roi accorde le monopole du commerce à ceux qui s'engagent à défrayer les coûts relatifs à de tels établissements. Cependant, cela ne fait pas l'unanimité et certains préfèrent que ce commerce demeure libre.\n" +
                        "\n" +
                        "En 1598, on installe un groupe de colons sur l'Île de Sable au large de la Nouvelle-Écosse actuelle. C'est un échec lamentable. En 1600, Pierre de Chauvin fonde un poste de traite à Tadoussac, au confluent du fleuve Saint-Laurent et de la rivière Saguenay. Le premier hiver décime la presque totalité de la petite population. C'est pour s'éloigner de ce lieu inhospitalier qu'en 1604, le huguenot Pierre Dugua de Mons, qui obtient le monopole du commerce des fourrures, tente d'établir une colonie d'abord à l'embouchure du fleuve Sainte-Croix. Il est accompagné de Samuel de Champlain et de Jean de Poutrincourt. Cette colonie ne survivra pas en raison de la rudesse de l'hiver et du manque d'eau douce. La moitié des colons meurt à l'hiver de 1605 et il est décidé de relocaliser le groupe à un autre endroit. Cet autre endroit, cette fois situé près de la Baie de Fundy, sera nommé Port-Royal (aujourd'hui la région d'Annapolis Royal, Nouvelle-Écosse). Cette étape de la colonisation se traduit par l'établissement de la première colonie française en Nouvelle-France, qui deviendra l'Acadie et donnera naissance à son peuple, les Acadiens.\n" +
                        "\n" +
                        "Faute de finances, les colons quitteront les lieux en 1607. En 1610, Jean de Poutrincourt, deuxième gouverneur de l'Acadie, avec son fils de 19 ans, Charles de Biencourt, Claude de Saint-Étienne de la Tour et son fils de 14 ans Charles de Saint-Étienne de la Tour, un prêtre catholique et d'autres colons français se sont rendus à l'habitation.\n" +
                        "\n" +
                        "En 1613, l'habitation a été attaquée par des colons anglais de la Virginie. Plusieurs colons français sont tués et d'autres sont enlevés. Le fort et les marchandises sont détruits. Biencourt, qui était en France pour recueillir des approvisionnements, est retourné à Port-Royal le printemps suivant. Il fut obligé de retourner en France avec les colons survivants. Charles de Biencourt et Charles de la Tour sont restés parmi les Micmacs, s'engageant dans l'industrie de la fourrure. Biencourt est mort en 1623.\n" +
                        "\n" +
                        "Pendant cette période, Samuel de Champlain qui est à l'emploi de De Monts, en profite pour faire plusieurs explorations. Il explore la côte atlantique entre l'Acadie et le Cap Cod puis il remonte le Saint-Laurent jusqu'à l'emplacement actuel de Montréal. En remontant le fleuve, il a remarqué un endroit que les Iroquoïens du temps de Jacques Cartier nommaient Stadaconé et que les Montagnais de son temps appelaient «Kébec». Le grand explorateur avait pris bonne note des avantages de ce lieu.\n" +
                        "\n" +
                        "La fondation de la ville de Québec (1608)\n" +
                        "\n" +
                        "Champlain, fondateur de la Nouvelle-France, tel qu'on le représente traditionnellement. Cependant, il n'existe pas de vrai portrait de Champlain, cette image serait celle d'un contrôleur des finances peu scrupuleux, Particelli d'Émery.\n" +
                        "La colonisation de la Nouvelle-France commence donc véritablement par la fondation de la ville de Québec par Samuel de Champlain en 1608. Champlain, qui fut d'abord impliqué dans des activités de pêche dans le golfe du fleuve Saint-Laurent, comprend vite l'intérêt du commerce des fourrures. Pour faciliter ce commerce, il rencontre un groupe important de Montagnais au poste de traite saisonnier de Tadoussac en 1603 où il scelle ni plus ni moins une alliance avec ce groupe Algonquin. Il réalise l'importance de disposer d'un établissement permanent. La ville de Québec devient donc le premier véritable établissement français permanent en Amérique. Ce site qu'il avait remarqué en 1603 comporte selon lui de nombreux avantages. D'abord, il y a abondance de fourrures. Ensuite, cela semble un endroit facile à défendre à cause de la hauteur du Cap Diamant. De plus, le fleuve étant étroit, on peut en contrôler l'accès plus facilement. Enfin, il y a beaucoup de terres fertiles. La colonisation commence donc mais elle progresse à pas de tortue. Il y érige donc l'Habitation.\n" +
                        "La colonisation progresse lentement parce que le système mis en place n'est pas adéquat. Le gouvernement français ne veut pas vraiment investir. Il confie donc le développement de sa colonie à une compagnie de commerce en lui octroyant l'exclusivité de la traite des fourrures. En retour, cette compagnie doit peupler le territoire. Pour une compagnie, il n'est pas très tentant d'utiliser une bonne partie de ses profits pour créer une colonie pour le roi. Pour cette raison, les compagnies qui se sont succédé ont toujours négligé leurs obligations et la Nouvelle-France ne se développait pas. Champlain met beaucoup d'énergie pour faire connaître tout le potentiel du nouveau territoire. En 1618, il soumet un mémoire aux autorités françaises dans lequel il fait l'inventaire de toutes les ressources de la colonie. Ses efforts finissent par porter fruit et le Cardinal de Richelieu s'intéresse à la Nouvelle-France. Une nouvelle compagnie voit le jour. C'est la Compagnie des Cent-Associés, la plus sérieuse tentative de développement à prendre place dans cette Nouvelle-France de 1627.\n" +
                        "\n" +
                        "Régime des compagnies à charte (1627-1662)\n" +
                        "\n" +
                        "En 1627, le cardinal de Richelieu confie à la Compagnie des Cent-Associés un monopole sur la traite des fourrures en échange d'un engagement à coloniser la Nouvelle-France. La tentative est importante. Chaque actionnaire investit 3 000 livres ce qui fait un capital de départ intéressant de 300 000 livres2. La compagnie obtient le monopole pour 15 ans et pendant ces années, elle doit amener 4 000 personnes dans la colonie. Le roi de France tente donc encore de coloniser la Nouvelle-France sans y investir d'argent. La nouvelle compagnie semble bien intentionnée. En effet, dès 1629, elle amène 400 personnes mais la flotte tombe au mains des Anglais dans le golfe Saint-Laurent. Malgré ces difficultés, il convient de saluer le rôle de Louis Hébert (colon) installé dès 1617 à Québec ainsi que la colonisation pionnière organisée par Robert Giffard. Arrivé en 1634 avec un premier groupe de migrants originaires de Mortagne-au-Perche, de Tourouvre et des environs, ce dernier peut être considéré comme l'initiateur de l'implantation de colons venus du Perche. Quoique peu nombreux, ceux-ci seront parmi les tout premiers à défricher et à générer une activité agricole régulière sur les rives du Saint-Laurent.\n" +
                        "\n" +
                        "La Compagnie des Cent-Associés ne se releva pas pour autant de ses revers financiers. De plus, la traite est rendue difficile à cause de l'hostilité croissante des Iroquois. Pratiquement ruinée, la Compagnie des Cent-Associés cède son monopole à la Compagnie des Habitants en 1645.\n" +
                        "Durant le régime des compagnies à charte, les villes de Trois-Rivières et de Montréal sont fondées en 1634 et en 1642 respectivement. Cependant la colonie reste très peu peuplée et, en 1662, 54 ans après la fondation de la ville de Québec, la colonie ne compte que 3 000 personnes. Les causes de la lenteur de la colonisation sont :\n" +
                        "l'absence d'une force de répulsion en France qui encouragerait certains Français à émigrer ;\n" +
                        "la faible force d'attraction de la colonie à cause du climat rigoureux du Québec ;\n" +
                        "l'hostilité iroquoise. (Cinq-Nations) ;une économie basée sur une seule ressource qui ne demande pas beaucoup de main-d'œuvre.\n" +
                        "l'impossibilité pour des milliers d'huguenots de s'établir sur les terres françaises d'Amérique, surtout après la révocation de l'édit de Nantes, alors que ceux qui ont traversé l'Atlantique se sont plutôt installés en Nouvelle-Angleterre, par le biais des Pays-Bas ou de la Grande-Bretagne.\n" +
                        "Le Gouvernement royal (1663-1760)\n" +
                        "\n" +
                        "Carte de la Nouvelle-France dédiée à Colbert (XVIIe siècle).\n" +
                        "\n" +
                        "En 1663, le roi de France, Louis XIV, constatant l'échec du système des compagnies, prend en main le développement de la Nouvelle-France en faisant une colonie royale. Le roi Soleil, de concert avec son ministre de la Marine Jean-Baptiste Colbert, met en place de nouvelles structures administratives. Il conserve le poste de gouverneur tout en précisant son rôle, et introduit une nouvelle fonction, celle de l'intendant. À ce poste, le roi nomme Jean Talon. Enfin, il met sur pied le Conseil souverain aussi appelé le Conseil supérieur.\n" +
                        "\n" +
                        "Même si en dépit de toutes ces mesures, la colonisation progresse, somme toute, assez peu, le visage de la Nouvelle-France est complètement modifié pendant toutes ces années. En effet, grâce aux politiques de Jean Talon, la population commence à augmenter de façon appréciable. Évidemment, cela n'a rien à voir avec l'accroissement démographique extraordinaire des colonies anglaises du sud qui menacent de plus en plus l'existence de la colonie française. Au recensement de 1666, on dénombre 3 215 personnes. En 1760, on retrouve environ 70 000 personnes en Nouvelle-France. Pendant toutes ces années, la colonie reçoit un peu moins de 10 000 immigrants ce qui veut dire que l'essentiel de l'accroissement démographique est dû à la natalité.\n" +
                        "\n" +
                        "Le commerce des fourrures demeure toujours le moteur de l'activité économique même si les intendants s'évertuent à tenter de diversifier l'économie. Cette activité amène les Canadiens et les Français à explorer le centre de l'Amérique du Nord jusqu'au golfe du Mexique et à y construire des postes de commerce des fourrures et des places fortifiées pour défendre les postes de commerce des fourrures. À la suite de ces explorations, le territoire de la Nouvelle-France atteint son expansion maximale. Il est de dimension continentale. Il est beaucoup plus vaste que le territoire des colonies britanniques qui sont situées le long de la côte Est de l'Amérique du Nord. Cependant, comme le territoire français est peu peuplé, il est donc très vulnérable.\n" +
                        "Pendant toutes ces années également, il y eut quatre conflits entre les colonies anglaises et la colonie française. Ce fut d'abord la guerre de la Ligue d'Augsbourg en Europe ou Première Guerre intercoloniale en Amérique (1689-1697). Ensuite, ce fut la guerre de Succession d'Espagne, la Deuxième Guerre intercoloniale (1702-1713). À la suite de cette guerre désastreuse pour la Nouvelle-France, la France entreprit la construction de la forteresse de Louisbourg sur l'Île Royale. Puis il y eut la guerre de Succession d'Autriche, la Troisième Guerre intercoloniale (1744-1748). Enfin, ce fut la guerre de Sept Ans (1756-1763), mieux connue au Québec comme étant la guerre de la Conquête (1754-1760), qui consacre la défaite définitive de la Nouvelle-France.\n");
                break;

            case 9:
                textView_hist.setText("Histoire du Saskatchewan\n" +
                        "\n" +
                        "Avant l'arrivée des Européens, la Saskatchewan est peuplée par les Athabascans, les Algonquiens et les Sioux. Le premier Européen en Saskatchewan est Henry Kelsey en 1690, qui descend la rivière Saskatchewan, cherchant une traite de fourrure avec les autochtones. Le premier établissement européen est un comptoir de la Compagnie de la Baie d'Hudson à Cumberland House, fondé par Samuel Hearne en 1774.\n" +
                        "\n" +
                        "Le peuplement de la région, qui fait alors partie des Territoires du Nord-Ouest, s'accélère pendant les années 1870 grâce à la construction du Canadien Pacifique et à la concession de terres gratuites aux colons volontaires. La Police Montée du Nord-ouest établit plusieurs postes à travers la Saskatchewan moderne.\n" +
                        "\n" +
                        "On déplace les autochtones par la force vers des réserves et les métis qui s'y sont établis, dirigés par Louis Riel, mènent la Rébellion du Nord-Ouest afin de former leur propre gouvernement indépendant du Canada. Riel se rend deux mois plus tard et est reconnu coupable de trahison.\n" +
                        "\n" +
                        "Le peuplement de la Saskatchewan continue par le biais du chemin de fer ; avec la croissance de sa population, elle devient une province en 1905. Mis à part les Canadiens-français qui viennent du pays, plusieurs peuples d’Europe contribuent au peuplement de la Saskatchewan. Aujourd'hui, la majorité de la population est composée des descendants d’Allemands, d’Anglais, d’Écossais, d’Irlandais, d’Ukrainiens, de Norvégiens, de Polonais, de Hollandais, de Suédois, etc. qui s'établissent dans la région à l’époque de la colonisation.");
                break;

            case 10:
                textView_hist.setText("Histoire de Terre Neuve et Labrador\n" +
                        "\n" +
                        "Précurseurs asiatiques\n" +
                        "\n" +
                        "Les premiers signes de présence humaine sur l’île de Terre-Neuve datent d’environ 6 000 ans av. J.-C. et relèvent de la « Civilisation archaïque maritime », culture de pêcheurs et de chasseurs d’animaux marins. On note le développement du travail du bois, de tombeaux à tumulus (site de L'Anse Amour) et un usage abondant de l’ocre rouge, sur les côtes des provinces maritimes actuelles et de Terre-Neuve. Cette civilisation s’éteint vers -2000, peut-être à la suite de la submersion du plateau continental à cette époque.\n" +
                        "Vers 850 av. J.-C. arrivent les paléo-inuits qui occupent l’île durant environ 700 ans. D'origine asiatique (Sibérie), ces Inuits ont émigré il y a plusieurs milliers d'années en traversant le détroit de Béring et en s’établissant en Amérique du Nord. Ils sont supplantés par les représentants de la Culture de Dorset et, simultanément par ceux de la culture « Recent Indian », ancêtres possibles des Béothuks. Ces deux cultures occupent l’île pendant le millénaire suivant.\n" +
                        "\n" +
                        "Les Vikings\n" +
                        "\n" +
                        "Les côtes de Terre-Neuve furent explorées pour la première fois par des Européens au début du Xe siècle. En effet, des vestiges vikings ont été retrouvés à L'Anse aux Meadows, à l’extrémité nord de l'île à la suite des fouilles menées par Helge Ingstad et son épouse, Anne Stine, en 1960. Selon la datation par le carbone 14, l'implantation n'aurait servi que durant une vingtaine d'années1. La dernière mention d'une expédition groenlandaise vers l'Amérique se trouve dans les annales islandaises de 1347 où il est décrit qu'un bateau du Groenland revenait d'un voyage au Markland2. Deux sagas islandaises, la Grœnlendinga saga ainsi que la Saga d'Erik le Rouge, racontent les découvertes vikings de l'Amérique du Nord. Cependant, il reste à ce jour impossible de dire avec certitude que les trouvailles archéologiques de l'Anse aux Meadows sont les restes des habitations des protagonistes de ces sagas. Terre-Neuve a souvent été associée au Vinland, une terre nommée par Leif Ericson au cours de son périple le long des côtes de l'Amérique du Nord, mais la situation précise de cet endroit fait encore l'objet de nombreux désaccords.\n" +
                        "\n" +
                        "Pêcheurs et explorateurs européens\n" +
                        "\n" +
                        "Les terres découvertes par Jean Cabot, tiré de Navigationi et viaggi de Giovanni Battista Ramusio (Venise, 1583).\n" +
                        "Il court sur la fréquentation des Grands Bancs plusieurs récits qui veulent y faire remonter la présence de pêcheurs européens au début du XVe siècle : outre certaines interprétations des voyages du Portugais João Vaz Corte-Real vers l’île de Bacalao, la source principale de ces allégations sur « des pêcheurs bretons de Paimpol et de Saint-Malo, des marins normands de Barfleur et de Dieppe, enfin ceux du Pays basque » paraît être l'ouvrage de Charles Desmarquets3. Ce récit a été repris complaisamment par plusieurs auteurs depuis, notamment Jean Mauclère4 et Samivel.\n" +
                        "\n" +
                        "La première expédition clairement documentée date de 1497 quand le pilote vénitien naturalisé anglais Jean Cabot 6 explore la région pour le compte de l’Angleterre et découvre les Grands Bancs de morue de Terre-Neuve. La pêche et l’exploitation des richesses naturelles marqueront le développement de l’île jusqu’à nos jours.\n" +
                        "\n" +
                        "En 1501, Gaspar Corte-Real explore la côte est de l’Amérique du Nord pour le compte du Portugal. Il capture 57 autochtones du Labrador ou de Terre-Neuve pour les ramener au Portugal.\n" +
                        "\n" +
                        "En 1502 des pêcheurs anglais commencent à fréquenter les bancs de Terre-Neuve, suivis par les Normands en 1506, les pêcheurs bretons de Dahouët en 1510 (à la suite de leur découverte de l'île Cap-Breton en 1504), de Bréhat en 1514, de Saint-Brieuc en 1516 puis des vaisseaux d’à peu près toutes les nations européennes ayant une façade sur l’Atlantique.\n" +
                        "\n" +
                        "Vers 1530, 50 navires de pêche européens viennent chaque année pêcher la morue, amenant une population saisonnière de 1 250 personnes. Simultanément, les Inuits sur une partie du territoire de Terre-Neuve entrent en conflit avec les Beothuks, ainsi qu’avec les pêcheurs blancs. À partir de 1580, les affrontements sont endémiques entre Inuits et Européens.\n" +
                        "\n" +
                        "Au cœur des rivalités coloniales\n" +
                        "\n" +
                        "Les prises de possessions anglaises\n" +
                        "En 1578, la reine d'Angleterre Élisabeth concédait à Sir Humphrey Gilbert, beau-frère de son favori, Sir Walter Raleigh, « tous pays lointains païens et barbares non actuellement possédés par prince ou peuple chrétien ».\n" +
                        "Le 5 août 1583, Sir Humphrey Gilbert prit officiellement possession de Terre-Neuve. La plupart des puissances européennes, dont la France, n'avaient pas voulu reconnaître cette prise de possession. Les Anglais n'y installèrent pas moins leurs postes de pêche. Ce geste sera suivi, deux ans plus tard, par la destruction de la flotte de pêche espagnole par l’amiral Drake.\n" +
                        "\n" +
                        "C'est en 1610 que John Guy, marchand anglais de Bristol, établissait dans la baie de la Conception la première colonie permanente à Terre-Neuve. Il fonde la « Newfoundland Company » (à Cupids) malgré une interdiction de la Couronne. Le gouvernement britannique refusera le statut de colonie à Terre-Neuve durant deux siècles. La Newfoundland Company fera faillite en 1631.\n" +
                        "En 1611 le pirate Peter Easton construit un fort à Harbour Grace et, de là, écume l’Atlantique. L’endroit deviendra une colonie de pêcheurs. Les combats commencent bientôt entre colons britanniques et Béothuks. Ceux-ci seront progressivement repoussés vers l’intérieur de l’île. Cette opération dans l'histoire actuelle serait considérée comme un crime de guerre.\n" +
                        "\n" +
                        "En 1623, George Calvert, qui deviendra plus tard Lord Baltimore, fonda un comptoir à Ferryland. Il tenta d'y établir des colons catholiques, et s'y retira en 1629.\n" +
                        "En 1634, un décret royal nomme gouverneur de Terre-Neuve le capitaine du premier navire à aborder au printemps. Ce sera longtemps le seul gouvernement de l'île.\n" +
                        "En 1638, le corsaire David Kirke prend possession des propriétés de Lord Baltimore à Ferryland et se nomme gouverneur. Il chasse les colons catholiques et les remplace par une centaine de nouveaux colons.\n" +
                        "De nombreux autres postes anglais surgiront par la suite sur les côtes orientales de Terre-Neuve.\n" +
                        "\n" +
                        "Les tentatives françaises\n" +
                        "Article détaillé : Plaisance (Nouvelle-France).\n" +
                        "À diverses reprises, à l'instigation d'influents armateurs français, ceux de La Rochelle en particulier, engagés dans l'industrie lucrative de la pêche sur les bancs de Terre-Neuve, la France avait vigoureusement protesté contre l'occupation de l'île par les Anglais.\n" +
                        "Ce n'est cependant qu'en 1635 que les Anglais accorderont aux pêcheurs français la permission de faire sécher leurs prises de morue sur les rivages de l'île. C'est alors que la pittoresque baie de Plaisance, située sur la côte sud de Terre-Neuve, deviendra le principal centre d'activité des pêcheries françaises du littoral de l'île.\n" +
                        "\n" +
                        "En 1655, le royaume de France nomme un premier gouverneur pour la petite colonie de pêcherie de Plaisance, Sieur de Kéréon.\n" +
                        "En 1658, à la faveur de l'état de guerre qui existait alors entre la France et l'Angleterre, Louis XIV octroya à Nicolas Gargot de La Rochette, capitaine au long cours, le port de Plaisance, à titre de fief héréditaire, ainsi qu'une vaste concession s'étendant sur vingt-six lieues de profondeur dans la région du sud de Terre-Neuve.\n" +
                        "\n" +
                        "En 1660, une commission royale désignait Nicolas Gargot comte de Plaisance et gouverneur de l'île.\n" +
                        "En 1662, Louis XIV fit fortifier Plaisance. Il nomma gouverneur, Thalour du Perron, qui fut assassiné l'année suivante, ainsi que son aumônier, par des soldats de sa garnison. Quelques mois plus tard, un commissaire du roi, le sieur de Monts, se rendant à Québec, fit débarquer à Plaisance un détachement de soldats ainsi que des vivres et des munitions.\n" +
                        "\n" +
                        "En 1663, le capitaine Nicolas Gargot, en conduisant à Québec le nouveau gouverneur de la Nouvelle-France, de Mésy, ainsi que le premier évêque de Québec, Monseigneur de Laval, laissa à Plaisance plusieurs familles de colons. De sorte qu'à l'époque, Plaisance était devenu un poste fortifié comptant quelque deux cents soldats, colons et pêcheurs.\n" +
                        "Carte britannique de la baie de Ferryland (1696).\n" +
                        "\n" +
                        "C'est alors que la France décida d'exercer sa souveraineté sur toute la partie sud de l'île de Terre-Neuve, du Cap Race au Cap Ray, ainsi que sur les îles du littoral.\n" +
                        "\n" +
                        "Au recensement de 1687, cette vaste région groupait 36 familles, la plupart d'origine basque, ainsi que 488 engagés, formant une population totale de 663 personnes, dont 256 demeuraient à Plaisance. À partir de 1662 une colonie française se développe dans la région de Plaisance. En 1690, l'Angleterre étant de nouveau en guerre avec la France, des flibustiers anglais, venus des côtes de l'est de Terre-Neuve, saccagèrent Plaisance, laissant la population dans le plus complet dénuement. La plupart des habitants ayant pris la fuite, il ne restait plus que 150 Français. De Prat était alors gouverneur.\n" +
                        "Le saccage des établissements anglais de Terre-Neuve en 1696 par les Français.\n" +
                        "\n" +
                        "L'année suivante, le 24 août 1691, les Anglais tentèrent un nouvel assaut contre Plaisance, mais le nouveau gouverneur, Joseph de Monbeton de Brouillan, les repoussa. Puis, après avoir formé de petits détachements, qu'accompagnaient des matelots basques, De Brouillan se porta à l'attaque des établissements britanniques de l'île qu'il dévasta à son tour, le mieux qu'il pût.\n" +
                        "Les Anglais s'attaquèrent de nouveau à Plaisance, en 1692 et en 1693, mais ils ne purent réussir à en déloger les Français.\n" +
                        "Puis, des secours étant arrivés de France et de Nouvelle-France, De Brouillan reprit l'offensive. Il partit le 9 septembre 1696, avec huit navires armés en provenance de Saint-Malo, en vue de s'emparer de Saint-Jean, chef-lieu de la côte anglaise de l'île. Ayant subi un échec, il était de retour à Plaisance le 17 octobre pour y trouver Pierre Le Moyne d'Iberville, arrivé avec sa flotte depuis le 12 septembre. C'est alors qu'une nouvelle expédition contre les postes anglais de Terre-Neuve s'organisa.\n" +
                        "\n" +
                        "Le 1er novembre 1696, d'Iberville partit de Plaisance avec 124 hommes en direction de Ferryland où De Brouillan avait également dirigé ses troupes par bateaux. Ils y effectuèrent leur jonction le 6 novembre. D'Iberville voulait d'abord s'attaquer à Carbonear, qui ne pouvait être pris que par surprise. De Brouillan s'y opposa, insistant pour que Saint-Jean soit assiégé en premier lieu. D'Iberville y acquiesça de mauvaise grâce.\n" +
                        "Arrivé devant Saint-Jean, le 28 novembre 1696, d'Iberville s'empara en quelques heures des deux premiers forts et assiégea le troisième qui capitula deux jours plus tard. Il lança ensuite des détachements de soldats contre le chef-lieu et les autres postes anglais qu'il pilla et détruisit au cours de l'hiver. De Brouillan, dont le concours lui avait été de peu de valeur, était retourné à Plaisance le 24 décembre.\n" +
                        "L'affrontement s'est soldé par la perte de deux cents soldats britanniques et par la capture de 1 838 autres. D'Iberville avait détruit les postes anglais échelonnés sur la côte orientale de Terre-Neuve, tels que Saint-Jean, le chef-lieu; Petty Harbour, Bay Bulls, Ferryland, Renews, Portugal Cove, Torbay, Cape Saint Francis, Fermeuse, Aquaforte, Quidi Vidi, Brigus, Heart's Content, Bay de Verde, Port Grove, Old Pelican et New Pelican. Ces divers établissements comprenaient alors une population de 2 321 personnes, dont 293 résidents et 2 028 engagés.\n" +
                        "Carbonear avait échappé à la destruction. Au printemps de 1697, d'Iberville s'apprêtait à attaquer Bonavista lorsque la Cour de France lui ordonna de se rendre à la Baie d'Hudson.\n" +
                        "\n" +
                        "Plaisance est cédé à la France.\n" +
                        "\n" +
                        "Les traités franco-anglais\n" +
                        "\n" +
                        "Levé des côtes de Terre-Neuve par James Cook (1775).\n" +
                        "Au traité de Ryswick, signé le 25 septembre 1697, la France et l'Angleterre se rendaient mutuellement leurs conquêtes en Amérique. Les Français conservaient l'Acadie et Plaisance et les Anglais leurs établissements de Terre-Neuve. De plus, la France obtenait, dans la région de Plaisance, un vaste territoire s'étendant sur les côtes du sud-ouest de l'île, comprenant les baies de Plaisance, de Fortune et de l'Hermitage. À l'époque, des postes français avaient été établis à Petit-Plaisance, Pointe-Verte, Baie-Fortune, Grand-Banc, Hermitage ainsi qu'aux îles Saint-Pierre.\n" +
                        "En 1704, de nouveau, un Français né en Nouvelle-France, Jacques Testard de Montigny, commandant une expédition de miliciens français et d’Abénaquis, ravage les établissements anglais.\n" +
                        "\n" +
                        "En 1705, les Français soutiennent l’installation de vingt-cinq familles micmaques à Plaisance.\n" +
                        "En 1708, c’est le début de la pêche commerciale au saumon dans les rivières par Georges Skiffington. Les Béothuks seront peu à peu évincés de l'accès à cette ressource dans les années qui suivent par les Britanniques.\n" +
                        "\n" +
                        "Pastour de Costebelle, qui fut le dernier gouverneur de Plaisance, dans un rapport au ministre Pontchartrain, daté du 28 octobre 1708, donne de Terre-Neuve, en particulier de la région de Plaisance, une description détaillée dont nous extrayons les passages suivants :\n" +
                        "« L'île de Terreneuve a trois cents lieues de circuit de Cap en Cap, sans approfondir dans l'enfoncement des baies dont elle est presque toute formée. Les terres étant extraordinairement coupées, la baie de Plaisance, qui se prend depuis le Cap Sainte-Marie, a 24 lieues de profondeur et 14 de largeur jusqu'au Cap Judas qui en forme l'ouverture. Elle renferme plusieurs îles dont les plus observées sont l'Île Rouge et l'Île Longue qui ne sont point habitées. Il n'y a aucun bois propre à la construction et à la mâture des vaisseaux, excepté du côté de la Régente dont on peut tirer quelques mâts de hunes pour des bâtiments de 28 à 30 canons. Encore faut-il couper vingt arbres pour en trouver un de bon. Il ne faut pas compter cet endroit à pouvoir fournir des bois d'une véritable utilité.\n");
                break;

            case 11:
                textView_hist.setText("Histoire du Territoire du Nord-Ouest\n" +
                        "\n" +
                        "C'est à partir de territoires appartenant à la Compagnie de la Baie d'Hudson que les Territoires du Nord-Ouest ont été crées.\n" +
                        "Les Territoires du Nord-Ouest sont créés en 1870, à la suite du transfert des territoires possédés par la Compagnie de la Baie d'Hudson au gouvernement du Canada. Avant ce transfert, la Compagnie possède une immense région recouvrant tout le Canada moderne à l'exception de la colonie de la Colombie-Britannique sur la côte pacifique, de la confédération canadienne (côtes des Grands Lacs, vallée du Saint-Laurent, tiers sud de l'actuel Québec et Provinces maritimes, de l'île de Terre-Neuve et de la côte du Labrador sur l'Atlantique et des îles de l'archipel arctique (mis à part la moitié sud de l'île de Baffin). La région est alors divisée en deux territoires, la Terre de Rupert (bassin de la baie d'Hudson) et le territoire du Nord-Ouest (bassin des océans Arctique et Pacifique).\n" +
                        "\n" +
                        "Après la création de la Confédération canadienne en 1867, son gouvernement achète la Terre de Rupert et le Territoire du Nord-Ouest à la Compagnie de la Baie d'Hudson en 1869. L'entrée de ces terres dans la confédération est retardée du fait de la rébellion de la rivière Rouge. Les Territoires du Nord-Ouest sont créés en 1870, en même temps que la province du Manitoba (alors une petite région carrée autour de Winnipeg).\n" +
                        "Le conseil temporaire du Nord-Ouest est créé en 1870. Le premier gouvernement du territoire s'installe en 1872.\n" +
                        "\n" +
                        "Réduction territoriale\n" +
                        "\n" +
                        "Jusqu'en 1912, des parties des Territoires du Nord-Ouest ont été progressivement rattachées à d'autres provinces, ou détachées pour former de nouvelles entités. Le district de Keewatin, au centre du territoire, a été séparé en 1876. Le Yukon a été formé sur sa partie occidentale en 1898 afin de mieux gérer les intérêts locaux lors de la ruée vers l'or du Klondike. Le sud-ouest du territoire a servi à créer les provinces de l'Alberta et de la Saskatchewan en 1905.\n" +
                        "Le Manitoba a été élargi en 1881, l'Ontario en 1874 et en 1889, et le Québec en 1898. Les frontières de ces trois provinces ont été largement repoussées vers le nord en 1912.\n" +
                        "\n" +
                        "En 1880, le Royaume-Uni a cédé au Canada la souveraineté sur les îles de l'archipel arctique. Elles ont été intégrées aux Territoires du Nord-Ouest. En 1905, le district de Keewatin a finalement été réintégré aux Territoires du Nord-Ouest.\n" +
                        "Au total, les districts suivants ont été détachés des Territoires du Nord-Ouest :\n" +
                        "Alberta (sud de l'Alberta) (1905);\n" +
                        "Assiniboia (en français Assiniboine) (sud de la Saskatchewan) (1905);\n" +
                        "Athabaska (nord de l'Alberta et de la Saskatchewan) (1905);\n" +
                        "Saskatchewan (centre de la Saskatchewan) (1905);\n" +
                        "Ungava (nord du Québec et intérieur du Labrador);\n" +
                        "\n" +
                        "Abitibi (1898) et Nouveau-Québec et Labrador (1912), ces frontières ayant été remodifiées lors du découpage de 1927 en faveur de Terre-Neuve, puis lors de plusieurs redécoupages internes des régions du Québec.\n" +
                        "En 1912, les Territoires du Nord-Ouest ne conservent que trois districts : Keewatin, Franklin et Mackenzie. Ils mesurent toutefois 3 439 296 km2, une superficie supérieure à celle de l'Inde.\n" +
                        "\n" +
                        "Lorsqu'en 1905, les provinces de l'Alberta et de la Saskatchewan sont détachées des Territoires du Nord-Ouest, la population de ces derniers passe de 160 000 à 17 000 habitants. Parmi ces derniers, 16 000 sont des Amérindiens qui ne disposent pas du droit de vote. Le territoire revient alors à son statut de 1870 et passe sous contrôle fédéral, gouverné depuis Ottawa.\n" +
                        "\n" +
                        "Entre 1907 et 1947, les Territoires du Nord-Ouest ne sont pas représentés à la Chambre des communes. En 1947, le district électoral du Yukon-Mackenzie est créé; il n'inclut que le district de Mackenzie. Le reste du territoire n'est pas représenté avant 1962, lors de la création du district électoral des Territoires du Nord-Ouest, en reconnaissance de l'accord du droit de vote aux Inuits en 1953.\n" +
                        "\n" +
                        "Les élections locales font leur réapparition en 1951, mais sous une forme partielle, le Conseil et l'Assemblée étant alors un mélange de membres élus et nommés. En 1975, le gouvernement territorial redevient un organe entièrement élu.\n" +
                        "Création du Nunavut\n" +
                        "En avril 1982 débute une longue période qui viendra amputer les Territoires du Nord-Ouest d'une partie de leur population et d'un vaste territoire géographique.\n" +
                        "En effet en cette année 1982, une majorité des habitants des Territoires votent en faveur d'une partition de la région, et le gouvernement fédéral donne son accord sous conditions sept mois plus tard. Après de longues négociations territoriales entre l'Inuit Tapiriit Kanatami et le gouvernement fédéral (débutées dès 1976), un accord est trouvé en septembre 1992.\n" +
                        "\n" +
                        "Le Nunavut a été créé à partir des Territoires du Nord-Ouest.\n" +
                        "En juin 1993, la Loi concernant l’Accord sur les revendications territoriales du Nunavut et la Loi sur le Nunavut sont votées par le parlement canadien. Après une période de transition, la partie orientale du territoire (incluant la totalité du district de Keewatin et une grande portion des deux autres) devient un territoire distinct sous le nom de Nunavut le 1er avril 1999.\n" +
                        "\n" +
                        "Remise en cause de la structure politique\n" +
                        "La dévolution du pouvoir est en constante évolution aux Territoire du Nord-Ouest, que ce soit par le transfert du siège du gouvernement territorial d’Ottawa à Yellowknife dans les années 1960 ou encore par la création du Nunavut dans les années 1990.\n" +
                        "De plus, la gouvernance par consensus a aussi été remise en cause par l'ancien Premier ministre Roland qui, en 2009, a établi 10 nouveaux principes du gouvernement par consensus sur les Territoires du Nord-Ouest.");
                break;

            case 12:
                textView_hist.setText("L’histoire du Yukon\n" +
                        "\n" +
                        "Les visiteurs ont de tout temps été les bienvenus au Yukon et cela n’a pas changé. Les milliers de lieux d’intérêt historique qui parsèment le territoire sont autant d’invitations à plonger dans le temps et à revivre avec nous les moments hauts en couleurs de notre passé.\n" +
                        "\n" +
                        "La Béringie\n" +
                        "\n" +
                        "L’ère glaciaire du Yukon occupe une place particulière dans l’histoire du territoire. Il y a plus de 20 000 ans, un pont terrestre reliait l’Asie à l’Amérique du Nord. Le mammouth laineux et le chat des cavernes parcouraient une vaste région libre de glace qu’on désigne sous le nom de Béringie. Contrairement au reste du continent qui était recouvert de glace, le Yukon était alors un refuge écologique pour la faune et la flore. Les légendes autochtones parlent de cette époque, des géants d’antan et de la création du monde à partir de terres englouties.\n" +
                        "\n" +
                        "Les premiers habitants du Yukon, dont la présence remonte à cette époque, sont venus d’Asie par un pont continental et fréquentaient un secteur non loin du lieu où se trouve aujourd’hui la collectivité d’Old Crow. Ils chassaient le mammouth, le bison, le cheval et le caribou. Avec le temps, ils se sont établis dans des peuplements plus permanents, dont certains existent toujours et comptent parmi les collectivités du territoire.\n" +
                        "\n" +
                        "La traite des fourrures\n" +
                        "\n" +
                        "Les explorateurs russes, en quête de fourrures et d’autres ressources, furent les premiers à visiter le Yukon au XVIIIe siècle, suivis par d’autres explorateurs venus d’Europe. Le nombre de ces deniers ne cessant de croître, les Autochtones se mirent à troquer des fourrures contre du tabac, des armes et d’autres marchandises. La traite des fourrures se développa peu à peu avec la venue de la Compagnie de la Baie d’Hudson et d’autres commerçants indépendants qui établirent des comptoirs de traite un peu partout au Yukon.\n" +
                        "\n" +
                        "La ruée vers l’or du Klondike\n" +
                        "\n" +
                        "En août 1896, trois hommes trouvèrent de l’or dans le ruisseau Bonanza près de Dawson, ce qui devait marquer le début de la légendaire ruée vers l’or du Klondike.\n" +
                        "Lorsque le bruit de cette découverte se répandit dans le reste du monde, des milliers d’aspirants prospecteurs prirent la route du nord. Au tournant du siècle, Dawson s’enorgueillissait d’être devenue l’agglomération la plus importante au nord de San Francisco et à l’ouest de Winnipeg.\n" +
                        "\n" +
                        "Le White Pass and Yukon Route\n" +
                        "\n" +
                        "Ce « chemin de fer qui vaut son pesant d’or » et dont la construction fut terminée en 1900 reliait Whitehorse, au Yukon, à Skagway, sur la côte de l’Alaska\n" +
                        "Jugée impossible à réaliser, cette voie ferrée fut construite en 26 mois seulement par des milliers d’hommes et au prix de 10 millions de dollars. On a dû littéralement faire sauter des pans entiers de la chaîne Côtière et utiliser 450 tonnes d’explosifs pour y arriver.\n" +
                        "Le White Pass and Yukon Route grimpe à près de 900 m (3 000 pi) sur une distance d’à peine 32 km (20 mi). Le chemin de fer serpente à flanc de montagnes aux parois abruptes, effectue des virages spectaculaires suspendus au-dessus du vide, et emprunte deux tunnels et quantité de ponts et de chevalets. Le pont cantilever qui le caractérise était, en 1901, le plus élevé du genre dans le monde.\n" +
                        "\n" +
                        "La route de l’Alaska\n" +
                        "\n" +
                        "Construite en 1942, cette route qui menait à la dernière frontière de l’Amérique du Nord devait servir à acheminer du matériel militaire. Terminée en 8 mois seulement et parcourant une distance de plus de 2 230 km jusqu’en Alaska, sa construction nécessita la contribution de plus de 30 000 soldats américains.\n" +
                        "La route de l’Alaska a transformé à jamais le visage du Yukon. Les bateaux et les trains cédèrent la place à un réseau routier plus efficace. L’expansion de Whitehorse en fit la ville la plus importante du territoire, puis la capitale en 1953.\n" +
                        "De nos jours, la route de l’Alaska est une route panoramique revêtue, bien entretenue et ouverte toute l’année.\n" +
                        "\n" +
                        "Le Yukon aujourd’hui\n" +
                        "\n" +
                        "Le Yukon actuel offre toutes les commodités de la vie moderne sans avoir sacrifié sa beauté naturelle.");
                break;
        }
        getSupportActionBar().setTitle("Histoire");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorRed)));
    }


}

